#pragma once

#include <array>
template<class T, size_t N>
class Pool {

	template<class T, size_t N>
	struct Node {
		Node* next;
		Node* previous;
		bool full;
		std::array<bool, N> free;
		std::array<T, N> elements;

		Node() {
			full = false;
			next = nullptr;
			previous = nullptr;
			for (bool& b : free){
				b = true;
			} 
		}
	};

	Node<T, N>* last;
	Node<T, N> initial;

	int nodes = 0;
	int maxNodes = 0;


public:
	T* alloc() {
		Node<T, N>* node = &initial;
		std::cout << "Elems :" << nodes << std::endl;

		while (node) {
			if (!node->full) {
				for (int i = 0; i < node->free.size(); i++) {
					if (node->free[i]) {
						node->free[i] = false;
						nodes++;
						return &(node->elements[i]);
					}
				}
			}
			node->full = true;
			if (node->next) {
				node = node->next;
				continue;
			}
			try {
				Node<T, N>* nodePtr = new Node<T, N>;
				node->next = nodePtr;
				nodePtr->previous = node;
				last = nodePtr;
				node = nodePtr;

			}
			catch (std::exception& e) {
				return nullptr;
			}
		};
	}

	void free(T* ptr) {
		if (ptr == nullptr) {
			return;
		}
		Node<T, N>* node = &initial;
		while (node) {
			intptr_t elPtr = reinterpret_cast<intptr_t>(&(node->elements));
			intptr_t p = reinterpret_cast<intptr_t>(ptr);
			if (p >= elPtr && p < (elPtr + sizeof(T) * N)) {
				p -= elPtr;
				int index = p / sizeof(T);

				if (node->free[index]) {
					throw std::exception();
				}

				node->free[index] = true;
				node->full = false;
				nodes--;
				return;
			}
			node->full = true;
			if (node->next) {
				node = node->next;
				continue;
			}
		};
	}
};
