#pragma once


/*
http://dmitrysoshnikov.com/compilers/writing-a-pool-allocator/
*/

struct Chunk {
	Chunk* next;
};

class PoolAllocator {
public:
	PoolAllocator(size_t chunksPerBlock)
		: mChunksPerBlock(chunksPerBlock) {}

	void* allocate(size_t size);
	void deallocate(void* ptr, size_t size);

private:
	/**
	 * Number of chunks per larger block.
	 */
	size_t mChunksPerBlock;

	/**
	 * Allocation pointer.
	 */
	Chunk* mAlloc = nullptr;

	/**
	 * Allocates a larger block (pool) for chunks.
	 */
	Chunk* allocateBlock(size_t size);
};