#pragma once

#include "Vec3.h"
#include "Mat4.h"

struct Mat3 {
union {
	float elements[3][3];
	Vec3 columns[3];
};
		
Mat3();
Mat3(float diagonal);

Mat3(const Mat4& m);

static Mat3 identity();


//static Mat4 translation(const Vec3& translation);
//static Mat4 rotation(float angle, const Vec3& axis);
//static Mat4 rotation(float roll, float pitch, float yaw);
//static Mat4 scale(const Vec3& scale);
//static Mat4 inverse(const Mat4& matrix);

static Mat3 transposed(const Mat3& m);
		
Mat3& multiply(const Mat3& other);

friend Mat3 operator*(const Mat3& left, const Mat3& right);
//friend Vec4 operator*(const Mat4& left, const Vec4& right);
//friend Vec3 operator*(const Mat4& left, const Vec3& right);

Mat3& operator*=(const float& right);
Mat3& operator/=(const float& right);
		
Mat3 operator*=(const Mat3& other);


std::string toString() const;
friend std::ostream& operator<<(std::ostream& stream, const Mat3& matrix);


};

