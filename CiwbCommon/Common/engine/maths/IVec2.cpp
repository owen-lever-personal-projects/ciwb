#pragma once
#include <iostream>
#include "IVec2.h"
#include <string>

IVec2::IVec2() {
	x = 0;
	y = 0;
}

IVec2::IVec2(int x, int y) {
	this->x = x;
	this->y = y;
}

IVec2::IVec2(int v) {
	this->x = v;
	this->y = v;
}

IVec2& IVec2::add(IVec2 other) {
	this->x += other.x;
	this->y += other.y;
	return *this;
}

IVec2& IVec2::subtract(IVec2 other) {
	this->x -= other.x;
	this->y -= other.y;
	return *this;
}

IVec2& IVec2::multiply(IVec2 other) {
	this->x *= other.x;
	this->y *= other.y;
	return *this;
}

IVec2& IVec2::divide(IVec2 other) {
	this->x /= other.x;
	this->y /= other.y;
	return *this;
}


std::ostream& operator<<(std::ostream& stream, const IVec2& vector) {
	stream << std::to_string(vector.x) << " : " << std::to_string(vector.y);
	return stream;
}

IVec2& IVec2::operator+=(const IVec2& right) {
	this->x += right.x;
	this->y += right.y;
	return *this;
}

IVec2& IVec2::operator*=(const IVec2& right) {
	this->x *= right.x;
	this->y *= right.y;
	return *this;

}
IVec2& IVec2::operator-=(const IVec2& right) {
	this->x -= right.x;
	this->y -= right.y;
	return *this;

}
IVec2& IVec2::operator/=(const IVec2& right) {
	this->x /= right.x;
	this->y /= right.y;
	return *this;
			
}
IVec2 operator+(const IVec2& left, const IVec2& right) {
	IVec2 result;
	result.x = left.x + right.x;
	result.y = left.y + right.y;
	return result;
}
IVec2 operator*(const IVec2& left, const IVec2& right) {
	IVec2 result;
	result.x = left.x * right.x;
	result.y = left.y * right.y;
	return result;
}
IVec2 operator-(const IVec2& left, const IVec2& right) {
	IVec2 result;
	result.x = left.x - right.x;
	result.y = left.y - right.y;
	return result;
}
IVec2 operator/(const IVec2& left, const IVec2& right) {
	IVec2 result;
	result.x = left.x / right.x;
	result.y = left.y / right.y;
	return result;
}
/*

friend IVec2 operator*(const IVec2& left, const float& right);
friend IVec2 operator/(const IVec2& left, const float& right);
*/

bool operator<(const IVec2& left, const IVec2& right) {
	if (left.x != right.x)
		return(left.x < right.x);

	return(left.y < right.y);

}

bool operator>(const IVec2& left, const IVec2& right) {
	if (left.x != right.x)
		return(left.x > right.x);

	return(left.y > right.y);
}

bool operator==(const IVec2& left, const IVec2& right) {
	return((left.x == right.x) & (left.y == right.y));
}