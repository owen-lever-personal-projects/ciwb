#include "Quaternion.h"

#include "MathsFunctions.h"

	
Quaternion::Quaternion(const float& s, const float& x, const float& y, const float& z) {
	this->s = s;
	this->x = x;
	this->y = y;
	this->z = z;
}

Quaternion::Quaternion(const float& s, const Vec3& vec) {
	this->s = s;
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}

Quaternion::Quaternion() {
	s = 1;
	x = 0;
	y = 0;
	z = 0;
}

Quaternion Quaternion::operator*(const Quaternion& other) const
{
	Quaternion result = Quaternion();

	result.r += s * other.s;
	result.r -= x * other.x;
	result.r -= y * other.y;
	result.r -= z * other.z;
		
	result.i += s * other.x;
	result.i += x * other.s;
	result.i += y * other.z;
	result.i -= z * other.y;

	result.j += s * other.y;
	result.j -= x * other.z;
	result.j += y * other.s;
	result.j += z * other.x;

	result.k += s * other.z;
	result.k += x * other.y;
	result.k -= y * other.x;
	result.k += z * other.s;

	return result;
}

/*
Quaternion operator+(const Quaternion& left, const Quaternion& right) {
	Quaternion r = left;
	r.add(right);
	return r;
}

Quaternion operator*(const Quaternion& left, const Quaternion& right) {
	Quaternion r = left;
	r.multiply(right);
	return r;
}

Quaternion operator-(const Quaternion& left, const Quaternion& right) {
	Quaternion r = left;
	r.subtract(right);
	return r;
}

Quaternion operator/(const Quaternion& left, const Quaternion& right) {
	Quaternion r = left;
	r.divide(right);
	return r;
}
*/

Quaternion& Quaternion::add(Quaternion other) {
	s += other.s;
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}
Quaternion& Quaternion::subtract(Quaternion other) {
	s -= other.s;
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

Quaternion& Quaternion::multiply(Quaternion other) {

	float tmpR(0);
	tmpR += s * other.s;
	tmpR -= x * other.x;
	tmpR -= y * other.y;
	tmpR -= z * other.z;

	float tmpI(0);
	tmpI += s * other.x;
	tmpI += x * other.s;
	tmpI += y * other.z;
	tmpI -= z * other.y;

	float tmpJ(0);
	tmpJ += s * other.y;
	tmpJ -= x * other.z;
	tmpJ += y * other.s;
	tmpJ += z * other.x;

	float tmpK(0);
	tmpK += s * other.z;
	tmpK += x * other.y;
	tmpK -= y * other.x;
	tmpK += z * other.s;

	r = tmpR;
	i = tmpI;
	j = tmpJ;
	k = tmpK;

	return *this;
}

Quaternion& Quaternion::divide(Quaternion other) {
	return *this;
}

Vec3 Quaternion::getVec3() const{
	return Vec3(x, y, z);
}

Quaternion& Quaternion::normalise() {
	float length = this->length();
	x /= length;
	y /= length;
	z /= length;
	w /= length;
	return *this;
}

float Quaternion::length() const{
	return sqrt(x * x + y * y + z * z + w * w);
}

Quaternion Quaternion::getInverse() const{
	return Quaternion(w, -x, -y, -z);
}

Quaternion Quaternion::getConjugate() const{
	return Quaternion(w, -x, -y, -z);
}

Vec3 Quaternion::operator*(const Vec3& right) const
{
	const float prodX = w * right.x + y * right.z - z * right.y;
	const float prodY = w * right.y + z * right.x - x * right.z;
	const float prodZ = w * right.z + x * right.y - y * right.x;
	const float prodW = -x * right.x - y * right.y - z * right.z;
	return Vec3(w * prodX - prodY * z + prodZ * y - prodW * x,
		w * prodY - prodZ * x + prodX * z - prodW * y,
		w * prodZ - prodX * y + prodY * x - prodW * z);
}



Quaternion Quaternion::fromEulerAngles(const float& roll, const float& pitch, const float& yaw){
	return fromEulerAngles(Vec3(roll, pitch, yaw));
}

Quaternion Quaternion::fromEulerAngles(const Vec3& vec) {

	float sx = sin(vec.x / 2);
	float cx = cos(vec.x / 2);

	float sy = sin(vec.y / 2);
	float cy = cos(vec.y / 2);

	float sz = sin(vec.z / 2);
	float cz = cos(vec.z / 2);

	float qw = cx * cy * cz + sx * sy * sz;
	float qx = sx * cy * cz - cx * sy * sz;
	float qy = cx * sy * cz + sx * cy * sz;
	float qz = cx * cy * sz - sx * sy * cz;

	Quaternion q(qw, qx, qy, qz);
	q.normalise();
	return q;
}

Vec3 Quaternion::toEulerAngles() const{
	Vec3 angles;
	toEulerAngles(angles.x, angles.y, angles.z);
	return angles;
}

void Quaternion::toEulerAngles(float& roll, float& pitch, float& yaw) const {

	float sinr_cosp = 2 * (w * x + y * z);
	float cosr_cosp = 1 - 2 * (x * x + y * y);
		
	float sinp = 2 * (w * y - z * x);

	float siny_cosp = 2 * (w * z + x * y);
	float cosy_cosp = 1 - 2 * (y * y + z * z);


	roll = atan2f(sinr_cosp, cosr_cosp);
	if (fabs(sinp) >= 1) pitch = copysignf(pi / 2, sinp);
	else pitch = asin(sinp);
	yaw = atan2f(siny_cosp, cosy_cosp);


}

Mat4 Quaternion::getMat4() const{

	float scale = 1/this->length();

	float qw, qx, qy, qz;

	qw = this->w * scale;
	qx = this->x * scale;
	qy = this->y * scale;
	qz = this->z * scale;

	Mat4 a;

	a.elements[0][0] = qw;
	a.elements[0][1] = qz;
	a.elements[0][2] = -qy;
	a.elements[0][3] = qx;

	a.elements[1][0] = -qz;
	a.elements[1][1] = qw;
	a.elements[1][2] = qx;
	a.elements[1][3] = qy;

	a.elements[2][0] = qy;
	a.elements[2][1] = -qx;
	a.elements[2][2] = qw;
	a.elements[2][3] = qz;

	a.elements[3][0] = -qx;
	a.elements[3][1] = -qy;
	a.elements[3][2] = -qz;
	a.elements[3][3] = qw;

	Mat4 b;

	b.elements[0][0] = qw;
	b.elements[0][1] = qz;
	b.elements[0][2] = -qy;
	b.elements[0][3] = -qx;

	b.elements[1][0] = -qz;
	b.elements[1][1] = qw;
	b.elements[1][2] = qx;
	b.elements[1][3] = -qy;

	b.elements[2][0] = qy;
	b.elements[2][1] = -qx;
	b.elements[2][2] = qw;
	b.elements[2][3] = -qz;

	b.elements[3][0] = qx;
	b.elements[3][1] = qy;
	b.elements[3][2] = qz;
	b.elements[3][3] = qw;



	return a.multiply(b);
}
