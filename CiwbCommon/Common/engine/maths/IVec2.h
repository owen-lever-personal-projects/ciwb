#pragma once
#include <iostream>



struct IVec2
{
	int x;
	int y;

	IVec2();
	IVec2(int x, int y);
	IVec2(int v);

	IVec2& add(IVec2 other);
	IVec2& subtract(IVec2 other);
	IVec2& multiply(IVec2 other);
	IVec2& divide(IVec2 other);


	friend std::ostream& operator<<(std::ostream& stream, const IVec2& vector);

	IVec2& operator+=(const IVec2& right);
	IVec2& operator*=(const IVec2& right);
	IVec2& operator-=(const IVec2& right);
	IVec2& operator/=(const IVec2& right);

	friend IVec2 operator+(const IVec2& left, const IVec2& right);
	friend IVec2 operator*(const IVec2& left, const IVec2& right);
	friend IVec2 operator-(const IVec2& left, const IVec2& right);
	friend IVec2 operator/(const IVec2& left, const IVec2& right);

	friend IVec2 operator*(const IVec2& left, const float& right);
	friend IVec2 operator/(const IVec2& left, const float& right);

	friend bool operator<(const IVec2& left, const IVec2& right);
	friend bool operator>(const IVec2& left, const IVec2& right);
	friend bool operator==(const IVec2& left, const IVec2& right);
};
