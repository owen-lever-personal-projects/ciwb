#pragma once
#include <iostream>
//#include "maths.h"
#include "Vec3.h"
#include "Vec4.h"
#include "Mat4.h"

	
	
struct Quaternion {
	union { float w; float r; float s;};
	union { float x; float i; };
	union { float y; float j; };
	union { float z; float k; };
		
	Quaternion();
	Quaternion(const float& s, const float& x, const float& y, const float& z);
	Quaternion(const Vec4& v);
	Quaternion(const float& s, const Vec3& v);


	Quaternion& add(Quaternion other);
	Quaternion& subtract(Quaternion other);
	Quaternion& multiply(Quaternion other);
	Quaternion& divide(Quaternion other);

	Vec3 getVec3() const;
	Mat4 getMat4() const;

	Quaternion& normalise();
	float length() const;

	static Quaternion cross(const Quaternion& a, const Quaternion& b);
	static float dot(const Quaternion& a, const Quaternion& b);
	static Quaternion normalised(const Quaternion& a);

	Quaternion getInverse() const;
	Quaternion getConjugate() const;

	friend std::ostream& operator<<(std::ostream& stream, const Quaternion& vector);

	Quaternion& operator+=(const Quaternion& right);
	Quaternion& operator*=(const Quaternion& right);
	Quaternion& operator-=(const Quaternion& right);
	Quaternion& operator/=(const Quaternion& right);

	//Transform operator*(const Transform& right) const;

	Quaternion operator+(const Quaternion& right) const;
	Quaternion operator*(const Quaternion& right) const;
	Quaternion operator-(const Quaternion& right) const;
	Quaternion operator/(const Quaternion& right) const;

	friend Quaternion operator*(const Quaternion& left, const float& right);
	friend Quaternion operator/(const Quaternion& left, const float& right);

	Vec3 operator*(const Vec3& right) const;

	friend bool operator<(const Quaternion& left, const Quaternion& right);
	friend bool operator==(const Quaternion& left, const Quaternion& right);

	static Quaternion fromEulerAngles(const float& roll, const float& pitch, const float& yaw);
	static Quaternion fromEulerAngles(const Vec3& vec);
	Vec3 toEulerAngles() const;
	void toEulerAngles(float& roll, float& pitch, float& yaw) const;
		

};