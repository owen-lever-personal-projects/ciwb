#include "Mat3.h"

	Mat3::Mat3(const Mat4& m) {

		this->elements[0][0] = m.elements[0][0];
		this->elements[0][1] = m.elements[0][1];
		this->elements[0][2] = m.elements[0][2];

		this->elements[1][0] = m.elements[1][0];
		this->elements[1][1] = m.elements[1][1];
		this->elements[1][2] = m.elements[1][2];

		this->elements[2][0] = m.elements[2][0];
		this->elements[2][1] = m.elements[2][1];
		this->elements[2][2] = m.elements[2][2];
	}

	Mat3 Mat3::transposed(const Mat3& m) {

		Mat3 r;

		r.elements[0][0] = m.elements[0][0];
		r.elements[0][1] = m.elements[1][0];
		r.elements[0][2] = m.elements[2][0];

		r.elements[1][0] = m.elements[0][1];
		r.elements[1][1] = m.elements[1][1];
		r.elements[1][2] = m.elements[2][1];

		r.elements[2][0] = m.elements[0][2];
		r.elements[2][1] = m.elements[1][2];
		r.elements[2][2] = m.elements[2][2];

		return r;

	}

	Mat3 operator*(const Mat3& left, const Mat3& right)
	{
		Mat3 result = left;
		return result.multiply(right);
	}



	Mat3::Mat3() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				elements[i][j] = 0.0f;
			}
		}

	}

	Mat3& Mat3::multiply(const Mat3& other) {
		float newElements[3][3];
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 3; x++) {
				float sum = 0.0f;
				for (int e = 0; e < 3; e++) {
					sum += elements[x][e] * other.elements[e][y];
				}
				newElements[x][y] = sum;

			}
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {

				elements[i][j] = newElements[i][j];
			}
		}
		return *this;
	}