#pragma once


#include "Quaternion.h"
#include "Mat4.h"
#include "Vec3.h"

	
struct Transform
{
	Vec3 position;
	Quaternion rotation;


	Transform();
		
	Transform(Vec3 position);
	Transform(Vec3 position, Vec3 rotation);
	Transform(Vec3 position, Quaternion rotation);
		

	Transform getInverse();
		
	Mat4 getMat4() const;
		
	Transform operator*(const Transform& right) const;
	Vec3 operator*(const Vec3& right) const;


};


