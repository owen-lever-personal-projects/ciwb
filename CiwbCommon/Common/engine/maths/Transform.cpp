#include "Transform.h"

const Vec3 defaultPos = Vec3(0);
const Quaternion defaultRot = Quaternion::fromEulerAngles(Vec3(0));

Transform::Transform() {
	this->position = defaultPos;
	this->rotation = defaultRot;
}

Transform::Transform(Vec3 position) {
	this->position = position;
	this->rotation = defaultRot;
}

Transform::Transform(Vec3 position, Vec3 rotation) {
	this->position = position;
	this->rotation = Quaternion::fromEulerAngles(rotation);
}

Transform::Transform(Vec3 position, Quaternion rotation) {
	this->position = position;
	this->rotation = rotation;
}

Transform Transform::getInverse() {
	const Quaternion& inv = rotation.getInverse();
	return Transform(inv * (-position), inv);
}


Transform Transform::operator*(const Transform& other) const
{
	
	//return Transform(this->position + this->rotation * other.position,
	//				this->rotation * other.rotation);
	

	const float prodX = rotation.w * other.position.x + rotation.y * other.position.z
		- rotation.z * other.position.y;
	const float prodY = rotation.w * other.position.y + rotation.z * other.position.x
		- rotation.x * other.position.z;
	const float prodZ = rotation.w * other.position.z + rotation.x * other.position.y
		- rotation.y * other.position.x;
	const float prodW = -rotation.x * other.position.x - rotation.y * other.position.y
		- rotation.z * other.position.z;

	return Transform(Vec3(position.x + rotation.w * prodX - prodY * rotation.z + prodZ * rotation.y - prodW * rotation.x,
		position.y + rotation.w * prodY - prodZ * rotation.x + prodX * rotation.z - prodW * rotation.y,
		position.z + rotation.w * prodZ - prodX * rotation.y + prodY * rotation.x - prodW * rotation.z),
		Quaternion(rotation.w * other.rotation.w - rotation.x * other.rotation.x
			- rotation.y * other.rotation.y - rotation.z * other.rotation.z,
			rotation.w * other.rotation.x + other.rotation.w * rotation.x
			+ rotation.y * other.rotation.z - rotation.z * other.rotation.y,
			rotation.w * other.rotation.y + other.rotation.w * rotation.y
			+ rotation.z * other.rotation.x - rotation.x * other.rotation.z,
			rotation.w * other.rotation.z + other.rotation.w * rotation.z
			+ rotation.x * other.rotation.y - rotation.y * other.rotation.x));


	
}

Vec3 Transform::operator*(const Vec3& other) const
{
	return (rotation * other) + position;
}

Mat4 Transform::getMat4() const{

	Mat4 result = rotation.getMat4();
	result.multiply(Mat4::translation(position));

	return result;
}