#pragma once
#include <iostream>

struct Vec2 
{
	float x;
	float y;

	Vec2();
	Vec2(const float& x, const float& y);
	Vec2(const float& v);

	Vec2& add(Vec2 other);
	Vec2& subtract(Vec2 other);
	Vec2& multiply(Vec2 other);
	Vec2& divide(Vec2 other);
		
	Vec2& normalise();
	float length() const;

	static Vec2 cross(const Vec2& a, const Vec2& b);
	static float dot(const Vec2& a, const Vec2& b);
	static Vec2 normalised(const Vec2& a);

	friend std::ostream& operator<<(std::ostream& stream, const Vec2& vector);

	Vec2& operator+=(const Vec2& right);
	Vec2& operator*=(const Vec2& right);
	Vec2& operator-=(const Vec2& right);
	Vec2& operator/=(const Vec2& right);

	friend Vec2 operator+(const Vec2& left, const Vec2& right);
	friend Vec2 operator*(const Vec2& left, const Vec2& right);
	friend Vec2 operator-(const Vec2& left, const Vec2& right);
	friend Vec2 operator/(const Vec2& left, const Vec2& right);

	friend Vec2 operator*(const Vec2& left, const float& right);
	friend Vec2 operator/(const Vec2& left, const float& right);

	friend bool operator<(const Vec2& left, const Vec2& right);
	friend bool operator==(const Vec2& left, const Vec2& right);
};
