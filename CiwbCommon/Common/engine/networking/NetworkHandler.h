#pragma once

#include "ENET/enet.h"
#include <thread>
#include <vector>

class NetworkUser {
public:
    ENetPeer* peer;

    NetworkUser(ENetPeer* peer) {
        this->peer = peer;
    }

    friend bool operator<(const NetworkUser& left, const NetworkUser& right) {
        return left.peer < right.peer;
    }
    friend bool operator>(const NetworkUser& left, const NetworkUser& right) {
        return left.peer > right.peer;
    }
    friend bool operator<=(const NetworkUser& left, const NetworkUser& right) {
        return left.peer <= right.peer;
    }
    friend bool operator>=(const NetworkUser& left, const NetworkUser& right) {
        return left.peer >= right.peer;
    }
    friend bool operator==(const NetworkUser& left, const NetworkUser& right) {
        return left.peer == right.peer;
    }
};
class NetworkHandler {
	
public:
    virtual void RecievePacket(NetworkUser user, int length, const char* data) = 0;
    virtual void OnConnect(NetworkUser user) = 0;
    virtual void OnDisconnect(NetworkUser user) = 0;

};

class ServerConnection {
    std::thread thread;
    NetworkHandler* handler;

    ENetHost* server;
    ENetAddress address;
    ENetEvent event;

    bool connected;

    void loop();
    bool running;
public:

    ServerConnection();
    ~ServerConnection();

    void setNetworkHandler(NetworkHandler* handler);
    bool setupServer(int port);
    void closeServer();
    bool sendPacket(NetworkUser, int length, const char* data);
    bool sendPacketToAll(int length, const char* data);
};

class ClientConnection {
    std::thread thread;
    NetworkHandler* handler;

    ENetHost* client;
    ENetAddress address;
    ENetEvent event;
    ENetPeer* peer;

    bool connected;

    void loop();
    bool running;
public:

    ClientConnection();
    ~ClientConnection();

    void setNetworkHandler(NetworkHandler* handler);
    bool connectToServer(const char* address, int port);
    void disconnectFromServer();
    bool sendPacket(int length, const char* data);

};

struct PacketHeaders {
    struct ToServer {
        static constexpr int16_t RegionDataRequest = 101;
        static constexpr int16_t PlayerPositionUpdate = 102;
        static constexpr int16_t BlockUpdate = 103;
        static constexpr int16_t ServerInfoRequest = 104;
        static constexpr int16_t PlayerStateRequest = 105;
        static constexpr int16_t RegionSubscriptionRequest = 106;
        static constexpr int16_t RegionSubscriptionCancelation = 107;

    };

    struct FromServer {
        static constexpr int16_t RegionDataResponse = 201;
        static constexpr int16_t PlayerPositionUpdate = 202;
        static constexpr int16_t BlockUpdate = 203;
        static constexpr int16_t ServerInfoResponse = 204;
        static constexpr int16_t PlayerStateResponse = 205;
        static constexpr int16_t RegionSubscriptionResponse = 206;
        static constexpr int16_t RegionSubscriptionCancelation = 207;
    };

};

class PacketReader {
    const uint8_t* data;
    int length;
    int offset;

public:

    int remaining() {
        return length - offset;
    }

    PacketReader(int length, const char* data) {
        this->length = length;
        this->data = reinterpret_cast<const uint8_t*> (data);
        offset = 0;
    }

    int8_t readChar() {
        int8_t r = data[offset];
        offset++;
        return r;
    }

    uint16_t readShort() {
        int size = 2;
        int16_t r = 0;
        int start = offset + size - 1;
        for (int i = 0; i < size; i++) {
            r <<= 8;
            r |= data[start - i];
        }
        offset += size;
        return r;
    }

    uint32_t readInt() {
        int size = 4;
        int32_t r = 0;
        int start = offset + size - 1;
        for (int i = 0; i < size; i++) {
            r <<= 8;
            r |= data[start - i];
        }
        offset += size;
        return r;
    }

    long readLong() {
        int size = 8;
        int64_t r = 0;
        int start = offset + size - 1;
        for (int i = 0; i < size; i++) {
            r <<= 8;
            r |= data[start - i];
        }
        offset += size;
        return r;
    }

    float readFloat() {
        int x = readInt();
        return *((float*)&x);
    }

    double readDouble() {
        long x = readLong();
        return *((double*)&x);
    }

};

class PacketBuilder {
    std::vector<char> data;
public:

    const char* getData() {
        return &data[0];
    }

    int getLength() {
        return data.size();
    }

    void writeChar(int8_t c) {
        data.push_back(c);
    }

    void writeShort(int16_t d) {
        for (int i = 0; i < 2; i++) {
            data.push_back(d & 0xFF);
            d >>= 8;
        }
    }

    void writeInt(int32_t d) {
        for (int i = 0; i < 4; i++) {
            data.push_back(d & 0xFF);
            d >>= 8;
        }
    }

    void writeLong(int64_t d) {
        for (int i = 0; i < 8; i++) {
            data.push_back(d & 0xFF);
            d >>= 8;
        }
    }

    void writeFloat(float d) {
        uint32_t x = *((int*)&d);
        writeInt(x);
    }

    void writeDouble(double d) {
        uint64_t x = *((long*)&d);
        writeLong(x);
    }


};