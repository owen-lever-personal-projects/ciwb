#include "NetworkHandler.h"

#include <iostream>

ServerConnection::ServerConnection() {

    if (enet_initialize() != 0)
    {
        std::cout << "An error occurred while initializing ENet" << std::endl;
    }

    atexit(enet_deinitialize);
    running = false;
}

ServerConnection::~ServerConnection() {
    closeServer();
}

void ServerConnection::setNetworkHandler(NetworkHandler* handler) {
	this->handler = handler;
}

bool ServerConnection::setupServer(int port) {
    address.host = ENET_HOST_ANY;
    address.port = port;

    server = enet_host_create(&address, 10, 1, 0, 0);

    if (!server) {
        std::cout << "Error creating server" << std::endl;
        return false;
    }
    running = true;
    thread = std::thread(&ServerConnection::loop, this);
    return true;
}

void ServerConnection::closeServer() {
    running = false;
    thread.join();
    enet_host_destroy(server);
}

bool ServerConnection::sendPacket(NetworkUser user, int length, const char* data) {
    ENetPacket* enetPacket = enet_packet_create(data, length, ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send(user.peer, 0, enetPacket);
    return true;

}

bool ServerConnection::sendPacketToAll(int length, const char* data) {
    ENetPacket* enetPacket = enet_packet_create(data, length, ENET_PACKET_FLAG_RELIABLE);
    enet_host_broadcast(server, 0, enetPacket);
    return true;
}

void ServerConnection::loop() {
    while (running) {
        while (enet_host_service(server, &event, 0) && running) {

            if (event.type == ENET_EVENT_TYPE_CONNECT) {
                if (handler) handler->OnConnect(NetworkUser(event.peer));
            }
            if (event.type == ENET_EVENT_TYPE_RECEIVE) {
                if (handler && event.packet) {
                    handler->RecievePacket(NetworkUser(event.peer), event.packet->dataLength, (char*)event.packet->data);
                    enet_packet_destroy(event.packet);
                }
            }
            if (event.type == ENET_EVENT_TYPE_DISCONNECT) {
                if (handler) handler->OnDisconnect(NetworkUser(event.peer));
            }

        }
    }
}
