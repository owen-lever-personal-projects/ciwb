#include "NetworkHandler.h"
#include <iostream>

void ClientConnection::loop() {

    while (running) {
        ENetEvent event;
        while (enet_host_service(client, &event, 0) && running) {
            if (event.type == ENET_EVENT_TYPE_RECEIVE) {
                if (event.packet && handler) {
                    NetworkUser user(NULL);
                    handler->RecievePacket(user, event.packet->dataLength, (char *)event.packet->data);
                    enet_packet_destroy(event.packet);
                }
            }
        }
    }
}

void ClientConnection::setNetworkHandler(NetworkHandler* handler) {
    this->handler = handler;
}

ClientConnection::ClientConnection() {
    running = false;
    connected = false;
    if (enet_initialize() != 0)
    {
        fprintf(stderr, "An error occurred while initializing ENet.\n");
    }

    client = enet_host_create(NULL, 1, 1, 0, 0);

    if (!client) {
        fprintf(stderr, "An error occurred while creating client.\n");
    }
}

bool ClientConnection::sendPacket(int length, const char* data) {
    //if (connected) {
        ENetPacket* enetPacket = enet_packet_create(data, length, ENET_PACKET_FLAG_RELIABLE);
        enet_peer_send(peer, 0, enetPacket);
        return true;

    //}
    return false;
}

bool ClientConnection::connectToServer(const char* ipAddress, int port) {
    enet_address_set_host(&address, ipAddress);
    address.port = port;

    peer = enet_host_connect(client, &address, 1, NULL);

    if (!peer) {
        std::cout << "No peers" << std::endl;
        return false;
    }

    if (enet_host_service(client, &event, 1000)) {
        if (event.type == ENET_EVENT_TYPE_CONNECT) {
            std::cout << "Connected" << std::endl;
        }
    }
    else {
        enet_peer_reset(peer);
        std::cout << "Not connected" << std::endl;
        return false;
    }


    running = true;
    thread = std::thread(&ClientConnection::loop, this);
    connected = true;
    return true;
}

void ClientConnection::disconnectFromServer() {
    if (running) {
        running = false;
        thread.join();
    }
    if (connected) {
        enet_peer_disconnect(peer, 0);

        while (enet_host_service(client, &event, 500)) {
            if (event.type == ENET_EVENT_TYPE_RECEIVE) {
                enet_packet_destroy(event.packet);
            }
            if (event.type == ENET_EVENT_TYPE_DISCONNECT) {
                std::cout << "Disconnected" << std::endl;
                break;
            }

        }
    }
    
    connected = false;
}

ClientConnection::~ClientConnection() {
    disconnectFromServer();
}
