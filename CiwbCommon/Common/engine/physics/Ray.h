#pragma once

#include "Common/engine/maths/maths.h"
#include "AABB.h"


using namespace CiwbEngine;

struct Ray {
	Vec3 position;
	Vec3 direction;

	Ray(Vec3 position, Vec3 direction) {
		this->position = position;
		this->direction = direction;
	}

    AABB getAABB() {
        AABB aabb;
        aabb.position = position;
        aabb.size = direction;

        if (aabb.size.x < 0) {
            aabb.size.x = -aabb.size.x;
            aabb.position.x -= aabb.size.x;
        }

        if (aabb.size.y < 0) {
            aabb.size.y = -aabb.size.y;
            aabb.position.y -= aabb.size.y;
        }

        if (aabb.size.z < 0) {
            aabb.size.z = -aabb.size.z;
            aabb.position.z -= aabb.size.z;
        }
        return aabb;
    }

    RayAABBCollisionResult interect(const AABB& other) {
        RayAABBCollisionResult result;

        float tmin, tmax, tymin, tymax, tzmin, tzmax;

        Vec3 min = other.position;
        Vec3 max = other.position + other.size;

        Vec3 invD = 1 / direction;

        bool xPositive = !(invD.x < 0);
        bool yPositive = !(invD.y < 0);
        bool zPositive = !(invD.z < 0);

        if (xPositive) {
            // Positive x ->
            tmin = (min.x - position.x) * invD.x;
            tmax = (max.x - position.x) * invD.x;
        }
        else {
            // Negative x ->
            tmin = (max.x - position.x) * invD.x;
            tmax = (min.x - position.x) * invD.x;
        }

        if (yPositive) {
            tymin = (min.y - position.y) * invD.y;
            tymax = (max.y - position.y) * invD.y;
        }
        else {
            // Negative y ->
            tymin = (max.y - position.y) * invD.y;
            tymax = (min.y - position.y) * invD.y;
        }
        
        // Block surface normal should face opposite direction of ray
        if (xPositive) result.normal = NormalDirection::NEGX;
        else result.normal = NormalDirection::POSX;

        if ((tmin > tymax) || (tymin > tmax))
            return result;


        if (tymin > tmin) {
            tmin = tymin;
            if (yPositive) result.normal = NormalDirection::NEGY;
            else result.normal = NormalDirection::POSY;
        }
        if (tymax < tmax)
            tmax = tymax;

        if (zPositive) {
            tzmin = (min.z - position.z) * invD.z;
            tzmax = (max.z - position.z) * invD.z;
        }
        else {
            // Negative z ->
            tzmin = (max.z - position.z) * invD.z;
            tzmax = (min.z - position.z) * invD.z;
        }

        if ((tmin > tzmax) || (tzmin > tmax))
            return result;


        if (tzmin > tmin) {
            tmin = tzmin;
            if (zPositive) result.normal = NormalDirection::NEGZ;
            else result.normal = NormalDirection::POSZ;
        }
        if (tzmax < tmax)
            tmax = tzmax;

        result.collides = true;
        result.min = tmin;
        result.max = tmax;


        return result;
	}

};