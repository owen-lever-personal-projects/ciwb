#pragma once

#include "Common/engine/maths/maths.h"

using namespace CiwbEngine;

struct CollisionResult {
	bool collides;

	float depth;
	Vec3 normalBtoA;


	CollisionResult() {
		collides = false;
		depth = INFINITY;
		normalBtoA = Vec3(0);
	}


};


enum struct NormalDirection : uint8_t {
	UNKNOWN = 1 << 0,
	POSX = 1 << 1,
	NEGX = 1 << 2,
	POSY = 1 << 3,
	NEGY = 1 << 4,
	POSZ = 1 << 5,
	NEGZ = 1 << 6
};

struct RayAABBCollisionResult {
	

	bool collides;
	float min;
	float max;
	NormalDirection normal;


	RayAABBCollisionResult() {
		collides = false;
		max = 0;
		min = 0;
	}

};