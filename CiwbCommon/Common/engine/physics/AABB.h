#pragma once

#include "Common/engine/maths/maths.h"
#include "CollisionResult.h"

using namespace CiwbEngine;

static const float epsilon = 0.0001;
struct AABB {
	Vec3 position;
	Vec3 size;



	CollisionResult collides(AABB& other) {
		CollisionResult r;

		float depth;

		depth = other.getTop() - this->getBottom();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, 1, 0);
			}
		}

		depth = this->getTop() - other.getBottom();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, -1, 0);
			}
		}

		depth = this->getRight() - other.getLeft();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(-1, 0, 0);
			}
		}

		depth = other.getRight() - this->getLeft();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(1, 0, 0);
			}
		}

		depth = other.getBack() - this->getFront();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, 0, 1);
			}
		}

		depth = this->getBack() - other.getFront();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, 0, -1);
			}
		}

		r.collides = true;

		return r;
	}

	CollisionResult checkXInterect(AABB& other) {
		CollisionResult r;

		float depth;

		depth = other.getTop() - this->getBottom();
		if (depth <= epsilon) return r;

		depth = this->getTop() - other.getBottom();
		if (depth <= epsilon) return r;

		depth = this->getRight() - other.getLeft();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(-1, 0, 0);
			}
		}

		depth = other.getRight() - this->getLeft();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(1, 0, 0);
			}
		}

		depth = other.getBack() - this->getFront();
		if (depth <= epsilon) return r;

		depth = this->getBack() - other.getFront();
		if (depth <= epsilon) return r;

		r.collides = true;

		return r;
	}

	CollisionResult checkYInterect(AABB& other) {
		CollisionResult r;

		float depth;

		depth = other.getTop() - this->getBottom();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, 1, 0);
			}
		}

		depth = this->getTop() - other.getBottom();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, -1, 0);
			}
		}

		depth = this->getRight() - other.getLeft();
		if (depth <= epsilon) return r;

		depth = other.getRight() - this->getLeft();
		if (depth <= epsilon) return r;

		depth = other.getBack() - this->getFront();
		if (depth <= epsilon) return r;

		depth = this->getBack() - other.getFront();
		if (depth <= epsilon) return r;

		r.collides = true;

		return r;
	}

	CollisionResult checkZInterect(AABB& other) {
		CollisionResult r;

		float depth;

		depth = other.getTop() - this->getBottom();
		if (depth <= epsilon) return r;

		depth = this->getTop() - other.getBottom();
		if (depth <= epsilon) return r;

		depth = this->getRight() - other.getLeft();
		if (depth <= epsilon) return r;

		depth = other.getRight() - this->getLeft();
		if (depth <= epsilon) return r;

		depth = other.getBack() - this->getFront();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, 0, 1);
			}
		}

		depth = this->getBack() - other.getFront();
		if (depth <= epsilon) return r;
		else {
			if (depth < r.depth) {
				r.depth = depth;
				r.normalBtoA = Vec3(0, 0, -1);
			}
		}

		r.collides = true;

		return r;
	}

	inline float getTop() {
		return position.y + size.y;
	}

	inline float getBottom() {
		return position.y;
	}

	inline float getLeft() {
		return position.x;
	}

	inline float getRight() {
		return position.x + size.x;
	}

	inline float getFront() {
		return position.z;
	}

	inline float getBack() {
		return position.z + size.z;
	}



};