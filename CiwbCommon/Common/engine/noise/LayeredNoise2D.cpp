

#include "LayeredNoise2D.h"

using namespace CiwbEngine;
using namespace Noise;

float LayeredNoise2D::noise(float x, float y) {

	perlin.offset = offset;
	perlin.scale = scale;

	float div = 0;
	float result = 0;

	float lac = 1;
	float pers = 1;

	for (int i = 0; i < levels; i++) {
		result += pers * perlin.noise(x * lac, y * lac);
		div += pers;
		lac *= lacunarity;
		pers *= persistence;
	}
	result /= div;

	float s = sin(result);
	float r = 1 - fabs(s);

	return result;
};