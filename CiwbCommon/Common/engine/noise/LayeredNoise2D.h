#pragma once

#include "NoiseFunction.h"
#include "PerlinNoise.h"


namespace CiwbEngine { namespace Noise {
	class LayeredNoise2D : public NoiseFunction2D {

		PerlinNoise2D perlin;
		long seed;

	public:


		LayeredNoise2D(long seed = 0, float lac = 2, float pers = 0.5, int levels = 2) : perlin(seed){
			this->lacunarity = lac;
			this->persistence = pers;
			this->levels = levels;

			this->scale = Vec2(1, 1);
			this->offset = Vec2(0, 0);

			this->seed = seed;
		}

		float NoiseFunction2D::noise(float x, float y);

		float lacunarity;
		float persistence;
		int levels;




	};
}}

