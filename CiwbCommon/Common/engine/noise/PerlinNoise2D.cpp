#include "PerlinNoise.h"

namespace CiwbEngine { namespace Noise {

	static Vec2 gradientVectors[8] = {
		Vec2(1, 1).normalise(),
		Vec2(1, -1).normalise(),
		Vec2(-1, -1).normalise(),
		Vec2(-1, 1).normalise(),
		Vec2(-1, 0).normalise(),
		Vec2(0, 1).normalise(),
		Vec2(1, 0).normalise(),
		Vec2(0, -1).normalise()
	};

	PerlinNoise2D::PerlinNoise2D(unsigned long seed) {
		this->seed = seed;
	}

	float PerlinNoise2D::noise(float x, float y) {
		x += offset.x;
		y += offset.y;

		x *= scale.x;
		y *= scale.y;

		int x0 = (int)floor(x);
		int y0 = (int)floor(y);

		x -= x0;
		y -= y0;

		float bl = Vec2::dot(Vec2(x, y), getGradient(x0, y0));
		float br = Vec2::dot(Vec2(x-1, y), getGradient(x0 + 1, y0));

		float tl = Vec2::dot(Vec2(x, y-1), getGradient(x0, y0 + 1));
		float tr = Vec2::dot(Vec2(x-1, y-1), getGradient(x0 + 1, y0 + 1));

		x = PerlinNoise::fade(x);
		y = PerlinNoise::fade(y);

		float b = PerlinNoise::interpolate(bl, br, x);
		float t = PerlinNoise::interpolate(tl, tr, x);

		float v = PerlinNoise::interpolate(b, t, y);
		return v;
	}

	Vec2 PerlinNoise2D::getGradient(int x, int y) {
		return gradientVectors[mix(seed, x, y)&7];
	}

	//int fibo(unsigned int index){int x=0;int y=1;int z=x+y;for(unsigned int i=0;i<index;i++){z=x+y;x=y;y=z;}returnx;}
}}


