#include "PerlinNoise.h"
#include <string>

#define NOISE_MAGIC_X    1619
#define NOISE_MAGIC_Y    31337
#define NOISE_MAGIC_Z    52591
#define NOISE_MAGIC_SEED 1013

float noise3d(int x, int y, int z, int seed)
{
	unsigned int n = (NOISE_MAGIC_X * x + NOISE_MAGIC_Y * y + NOISE_MAGIC_Z * z
		+ NOISE_MAGIC_SEED * seed) & 0x7fffffff;
	n = (n >> 13) ^ n;
	n = (n * (n * n * 60493 + 19990303) + 1376312589) & 0x7fffffff;
	return 1.f - (float)(int)n / 0x40000000;
}

inline float linearInterpolation(float v0, float v1, float t)
{
	return v0 + (v1 - v0) * t;
}

inline float biLinearInterpolationNoEase(
	float v00, float v10,
	float v01, float v11,
	float x, float y)
{
	float u = linearInterpolation(v00, v10, x);
	float v = linearInterpolation(v01, v11, x);
	return linearInterpolation(u, v, y);
}

float triLinearInterpolation(
	float v000, float v100, float v010, float v110,
	float v001, float v101, float v011, float v111,
	float x, float y, float z)
{
	float tx = CiwbEngine::Noise::PerlinNoise::fade(x);
	float ty = CiwbEngine::Noise::PerlinNoise::fade(y);
	float tz = CiwbEngine::Noise::PerlinNoise::fade(z);
	float u = biLinearInterpolationNoEase(v000, v100, v010, v110, tx, ty);
	float v = biLinearInterpolationNoEase(v001, v101, v011, v111, tx, ty);
	return linearInterpolation(u, v, tz);
}

float triLinearInterpolationNoEase(
	float v000, float v100, float v010, float v110,
	float v001, float v101, float v011, float v111,
	float x, float y, float z)
{
	float u = biLinearInterpolationNoEase(v000, v100, v010, v110, x, y);
	float v = biLinearInterpolationNoEase(v001, v101, v011, v111, x, y);
	return linearInterpolation(u, v, z);
}

namespace CiwbEngine { namespace Noise {

	static Vec3 gradientVectors[12] = {
		Vec3(1, 1, 0).normalise(),
		Vec3(1, -1, 0).normalise(),
		Vec3(-1, 1, 0).normalise(),
		Vec3(-1, -1, 0).normalise(),

		Vec3(1, 0, 1).normalise(),
		Vec3(1, 0, -1).normalise(),
		Vec3(-1, 0, 1).normalise(),
		Vec3(-1, 0, -1).normalise(),

		Vec3(0, 1, 1).normalise(),
		Vec3(0, 1, -1).normalise(),
		Vec3(0, -1, 1).normalise(),
		Vec3(0, -1, -1).normalise()

	};

	PerlinNoise3D::PerlinNoise3D(unsigned long seed) {
		this->seed = seed;
		scale = Vec3(1, 1, 1);
	}

	float noise3d_gradient(float x, float y, float z, int seed, bool eased)
	{
		// Calculate the integer coordinates
		int x0 = floor(x);
		int y0 = floor(y);
		int z0 = floor(z);
		// Calculate the remaining part of the coordinates
		float xl = x - (float)x0;
		float yl = y - (float)y0;
		float zl = z - (float)z0;
		// Get values for corners of cube
		float v000 = noise3d(x0, y0, z0, seed);
		float v100 = noise3d(x0 + 1, y0, z0, seed);
		float v010 = noise3d(x0, y0 + 1, z0, seed);
		float v110 = noise3d(x0 + 1, y0 + 1, z0, seed);
		float v001 = noise3d(x0, y0, z0 + 1, seed);
		float v101 = noise3d(x0 + 1, y0, z0 + 1, seed);
		float v011 = noise3d(x0, y0 + 1, z0 + 1, seed);
		float v111 = noise3d(x0 + 1, y0 + 1, z0 + 1, seed);
		// Interpolate
		if (eased) {
			return triLinearInterpolation(
				v000, v100, v010, v110,
				v001, v101, v011, v111,
				xl, yl, zl);
		}

		return triLinearInterpolationNoEase(
			v000, v100, v010, v110,
			v001, v101, v011, v111,
			xl, yl, zl);
	}

	//float PerlinNoise3D::noise(float x, float y, float z) {
	float mt_noise(float x, float y, float z, int seed) {
		bool eased = true;
		int x0 = floor(x);
		int y0 = floor(y);
		int z0 = floor(z);
		// Calculate the remaining part of the coordinates
		float xl = x - (float)x0;
		float yl = y - (float)y0;
		float zl = z - (float)z0;
		// Get values for corners of cube
		float v000 = noise3d(x0, y0, z0, seed);
		float v100 = noise3d(x0 + 1, y0, z0, seed);
		float v010 = noise3d(x0, y0 + 1, z0, seed);
		float v110 = noise3d(x0 + 1, y0 + 1, z0, seed);
		float v001 = noise3d(x0, y0, z0 + 1, seed);
		float v101 = noise3d(x0 + 1, y0, z0 + 1, seed);
		float v011 = noise3d(x0, y0 + 1, z0 + 1, seed);
		float v111 = noise3d(x0 + 1, y0 + 1, z0 + 1, seed);
		// Interpolate
		if (eased) {
			return triLinearInterpolation(
				v000, v100, v010, v110,
				v001, v101, v011, v111,
				xl, yl, zl);
		}

		return triLinearInterpolationNoEase(
			v000, v100, v010, v110,
			v001, v101, v011, v111,
			xl, yl, zl);
	}

	float PerlinNoise3D::noise(float x, float y, float z) {
		x += offset.x;
		y += offset.y;
		z += offset.z;

		x *= scale.x;
		y *= scale.y;
		z *= scale.z;

		return mt_noise(x, y, z, seed);

		int x0 = (int)floor(x);
		int y0 = (int)floor(y);
		int z0 = (int)floor(z);

		x -= x0;
		y -= y0;
		z -= z0;

		float tfl = noise3d(x	, y-1	, z-1, seed);
		float tfr = noise3d(x - 1, y-1	, z-1, seed);

		float tbl = noise3d(x	, y-1	, z, seed);
		float tbr = noise3d(x - 1, y-1	, z, seed);

		float bfl = noise3d(x	, y		, z - 1, seed);
		float bfr = noise3d(x - 1, y	, z - 1, seed);

		float bbl = noise3d(x	, y		, z, seed);
		float bbr = noise3d(x - 1, y	, z, seed);

		x = PerlinNoise::fade(x);
		y = PerlinNoise::fade(y);
		z = PerlinNoise::fade(z);

		float tb = PerlinNoise::interpolate(tbl, tbr, x);
		float tf = PerlinNoise::interpolate(tfl, tfr, x);
		float bb = PerlinNoise::interpolate(bbl, bbr, x);
		float bf = PerlinNoise::interpolate(bfl, bfr, x);

		float t = PerlinNoise::interpolate(tb, tf, z);
		float b = PerlinNoise::interpolate(bb, bf, z);

		float v = PerlinNoise::interpolate(b, t, y);
		//std::cout << (v + 1) / 2 << std::endl;
		return v;
	}

	Vec3 PerlinNoise3D::getGradient(int x, int y, int z) {
		int s = 0;
		srand(seed);
		s += rand();
		srand(s + x);
		s += rand();
		srand(s + y);
		s += rand();
		srand(s + z);
		return gradientVectors[rand() % 12];
	}



	
}}