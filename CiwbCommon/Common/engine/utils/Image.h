#pragma once
#include <string>
#include <FI/FreeImage.h>
#include <iostream>
#include <vector>

namespace CiwbEngine {

	class Image {

	public:


		Image(std::string filepath, bool flipped = false);

		~Image();

		int height;
		int width;
		BYTE* data;

	private:
		void load(std::string filepath, bool flipped);
		FIBITMAP* dib;


	};
}