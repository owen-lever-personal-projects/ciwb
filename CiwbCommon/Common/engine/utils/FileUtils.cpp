#include "FileUtils.h"

#include <filesystem>
#include <iostream>

std::vector<std::string> find_files(const std::string& dir, const std::string& ext) {
	std::vector<std::string> ret;
	for (auto& p : std::filesystem::directory_iterator(dir)) {
		if (p.is_regular_file()) {
			if (dir.find(ext)) {
				std::cout << "Match :" << std::endl;
				std::cout << dir << std::endl;
				std::cout << ext << std::endl;
			}
			std::cout << p.path().extension() << std::endl;
			auto path = p.path().string();
			ret.push_back(path);
		}
	}
	return ret;

}

std::string read_file(const std::string& filePath, const std::string& mode) {
	return read_file(filePath.c_str(), mode.c_str());
}

std::string read_file(const char* filePath, const char* mode) {
	FILE* file;
	fopen_s(&file, filePath, mode);

	fseek(file, 0, SEEK_END);
	unsigned long length = ftell(file);
	char* data = new char[length + 1];
	memset(data, '\0', length + 1);
	fseek(file, 0, SEEK_SET);
	fread(data, 1, length, file);
	fclose(file);
	std::string result(data);
	delete[] data;

	return result;
}