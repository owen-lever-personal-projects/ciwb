#pragma once
#include <string>
#include <vector>

std::vector<std::string> find_files(const std::string& dir, const std::string& ext = ".");

std::string read_file(const std::string& filePath, const std::string& mode = "rt");

std::string read_file(const char* filePath, const char* mode = "rt");