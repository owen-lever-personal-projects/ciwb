#pragma once

class Block {


public:
	
	int id;

	Block(int id) {
		this->id = id;
	}

	Block() {
		this->id = 0;
	}

	bool operator==(Block& other) {
		return this->id == other.id;
	}

};