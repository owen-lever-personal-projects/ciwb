#pragma once

#include "nlohmann/json.hpp"

#include <string>
#include <map>
#include "Common/engine/utils/FileUtils.h"

struct BlockFace {
	static const int NEGX = 0;
	static const int POSX = 1;
	static const int NEGY = 2;
	static const int POSY = 3;
	static const int NEGZ = 4;
	static const int POSZ = 5;

	static const int WEST = 0;
	static const int EAST = 1;
	static const int BOTTOM = 2;
	static const int TOP = 3;
	static const int NORTH = 4;
	static const int SOUTH = 5;
};

struct FaceTexData {
	union {
		int textures[6];
		struct {
			int west;
			int east;
			int bottom;
			int top;
			int north;
			int south;
		};
	};

	using json = nlohmann::json;
	NLOHMANN_DEFINE_TYPE_INTRUSIVE(FaceTexData, north, south, east, west, top, bottom);


	int getTexture(int index) {
		return textures[index];
	}

	FaceTexData(std::vector<int>& input) {
		for (int i = 0; i < 6; i++) {
			if (input.size() < i) {
				textures[i] = -1;
			}
			else {
				textures[i] = input[i];
			}
		}
	}

	FaceTexData(int nx, int px, int ny, int py, int nz, int pz) {
		this->textures[BlockFace::NEGX] = nx;
		this->textures[BlockFace::POSX] = px;
		this->textures[BlockFace::NEGY] = ny;
		this->textures[BlockFace::POSY] = py;
		this->textures[BlockFace::NEGZ] = nz;
		this->textures[BlockFace::POSZ] = pz;
	}

	FaceTexData(int tex) {
		this->textures[BlockFace::NEGX] = tex;
		this->textures[BlockFace::POSX] = tex;
		this->textures[BlockFace::NEGY] = tex;
		this->textures[BlockFace::POSY] = tex;
		this->textures[BlockFace::NEGZ] = tex;
		this->textures[BlockFace::POSZ] = tex;
	}

	FaceTexData(){
		this->textures[BlockFace::NEGX] = -1;
		this->textures[BlockFace::POSX] = -1;
		this->textures[BlockFace::NEGY] = -1;
		this->textures[BlockFace::POSY] = -1;
		this->textures[BlockFace::NEGZ] = -1;
		this->textures[BlockFace::POSZ] = -1;
	}
};




class BlockData {

public:

	using json = nlohmann::json;

	int blockId;
	std::string name;
	FaceTexData blockTexture;
	bool isSolid = false;
	bool isOpaque = false;
	Vec4 color;

	NLOHMANN_DEFINE_TYPE_INTRUSIVE(BlockData, blockId, blockTexture, name, isSolid, isOpaque);

	BlockData() {
		this->blockId = 0;
		this->blockTexture = FaceTexData(-1, -1, -1, -1, -1, -1);
		this->isSolid = false;
		this->isOpaque = false;
		this->color = Vec4(1, 1, 1, 1);
	}

	BlockData(const std::string& name, int id, const FaceTexData& tex, bool solid, bool opaque) {
		this->name = name;
		this->blockId = id;
		this->blockTexture = tex;
		this->isSolid = solid;
		this->isOpaque = opaque;
		this->color = Vec4(1, 1, 1, 1);
	}

};

class BlockDataManager {

	std::vector<BlockData> blocks;

	std::map<std::string, int> names;
	std::map<int, int> ids;

	BlockData null;

	void addBlock(const BlockData& block) {
		blocks.push_back(block);
		int index = blocks.size() - 1;
		names.insert(std::make_pair(block.name, index));
		ids.insert(std::make_pair(block.blockId, index));
	}

public:

	BlockDataManager() {
		null = BlockData("null", -1, FaceTexData(), false, false);

		using json = nlohmann::json;


		for (auto& path : find_files("data/blocks/")) {
			std::string file = read_file(path);
			json block = json::parse(file);
			addBlock(block.get<BlockData>());

		}
	}

	BlockData& getBlockWithName(const std::string& name) {
		if (names.count(name) != 0) {
			int index = names[name];
			return blocks[index];
		}
		return null;
	}

	BlockData& getBlockWithId(int id) {
		if (ids.count(id) != 0) {
			int index = ids[id];
			return blocks[index];
		}
		return null;
	}


};