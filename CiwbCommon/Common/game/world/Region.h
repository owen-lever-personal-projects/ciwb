#pragma once

#include "Block.h"
#include "../../engine/noise/NoiseFunction.h"
#include "Common/engine/maths/maths.h"
#include <list>
#include <vector>

#include "BlockData.h"
#include "Common/engine/memory/PoolAllocator.h"
#include "Common/engine/networking/NetworkHandler.h"
//#include "World.h"

using namespace CiwbEngine;
using namespace Noise;

static const int REGION_VERTICAL_SEGMENTS = 32;
static const int REGION_SEGMENT_HEIGHT = 32;
static const int REGION_WIDTH = 32;
static const int REGION_LENGTH = 32;
static const int REGION_HEIGHT = REGION_VERTICAL_SEGMENTS * REGION_SEGMENT_HEIGHT;
static const int TOTAL_VOLUME_BLOCKS = REGION_WIDTH * REGION_LENGTH * REGION_SEGMENT_HEIGHT;
static const int TOTAL_REGION_BLOCKS = REGION_WIDTH * REGION_LENGTH * REGION_HEIGHT;

class Segment {
	Block blocks[TOTAL_VOLUME_BLOCKS];


	static PoolAllocator allocator;

public:
	Block& operator[](int index) {
		return blocks[index];
	}

	/*static void* operator new(size_t size) {
		return allocator.allocate(size);
	}

	static void operator delete(void* ptr, size_t size) {
		return allocator.deallocate(ptr, size);
	}*/
};


class Volume {

	int blockCount = 0;
	Segment* blocks = nullptr;

public:
	Block operator[](int index) {
		index %= TOTAL_VOLUME_BLOCKS;
		if (blocks) {
			return (*blocks)[index];
		}
		return Block(0);
	}

	void placeBlock(Block block, int index) {

		index %= TOTAL_VOLUME_BLOCKS;

		if (blocks) {
			Block& old = (*blocks)[index];

			if (old.id) blockCount--;
			if (block.id) blockCount++;

			(*blocks)[index] = block;
		}
		else {
			if (block.id) {
				blocks = new Segment;
				if (blocks) {
					(*blocks)[index] = block;
					blockCount++;
				}
			}
		}

		if (blockCount == 0) {
			delete blocks;
			blocks = nullptr;
		}

	}

	Volume() {

	}

	~Volume() {
		delete blocks;
	}
};

class Region {
public:
	Region(IVec2 position, BlockDataManager& manager);

	BlockDataManager& manager;
	IVec2 position;

	Volume volumes[REGION_VERTICAL_SEGMENTS];
	bool updateMesh[REGION_VERTICAL_SEGMENTS];


	Block getBlock(int x, int y, int z);
	void placeBlock(Block block, int x, int y, int z);

	//int getBlockIdNull(int x, int y, int z);
	//int getTexture(int id, int face);

	void toPacket(PacketBuilder& builder);

	Region(BlockDataManager& manager, PacketReader& reader);

	static int getIndex(int x, int y, int z) {
		//int index = x + (z * REGION_WIDTH);
		//return blocks[index];
		int index = y;
		index *= REGION_WIDTH;
		index += x;
		index *= REGION_LENGTH;
		index += z;
		return index;
		// Ywl + Xl + z
	}

};