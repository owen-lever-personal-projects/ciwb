#pragma once
#include "Region.h"
#include "Common/engine/noise/LayeredNoise2D.h"
#include "Common/engine/utils/Base64.h"
#include "LIBNOISE/noise.h"

#include <fstream>
#include <sstream>

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include "Common/engine/utils/miniz.h"

using namespace noise;


class RegionFactory {



public:
	long seed;
	BlockDataManager& manager;

	virtual std::shared_ptr<Region> GetRegion(int x, int z) = 0;
	virtual void UnloadRegion(Region& region) = 0;

	RegionFactory(long seed, BlockDataManager& manager) : seed(seed), manager(manager) {

	}

};



class Biome {
public:

	Biome(const std::string& name, float height, float deviation, float cliffWeight) {
		this->name = name;
		this->height = height;
		this->deviation = deviation;
		this->cliffWeight = cliffWeight;
	}

	Biome(std::string&& name, float height, float deviation, float cliffWeight) {
		this->name = name;
		this->height = height;
		this->deviation = deviation;
		this->cliffWeight = cliffWeight;
	}

	std::string name;
	float height;
	float deviation;
	float cliffWeight;


};



class ClassicRegionFactory : public RegionFactory{


private:

	//std::shared_ptr<Region> GenerateRegion(int x, int z);
	//std::shared_ptr<Region> LoadRegion(int x, int z);

public:

	module::Perlin worldMap_;
	module::Perlin cliffMap_;

	PerlinNoise2D worldMap;
	LayeredNoise2D heightMap;
	PerlinNoise3D cliffMap;
	float xScale = 0.1 / pi;
	float zScale = 0.1 / pi;
	float yScale = 0.25;

	LayeredNoise2D biomeMap;

	Biome getBiomeAt(int xCoord, int yCoord) {

		float b = biomeMap.noise(xCoord, yCoord);

		if (b > 0.2) {
			return Biome("Hills", 128+128*(b), 40, 20);
		}
		else {
			return Biome("plains", 128, 60, 0);
		}




	}


	ClassicRegionFactory(long seed, BlockDataManager& manager) : RegionFactory(seed, manager){
		worldMap = PerlinNoise2D(seed-1);
		worldMap.scale = Vec2(0.05);

		heightMap = LayeredNoise2D(seed, 2, 0.3, 4);
		heightMap.scale = Vec2(0.003);

		worldMap_.SetSeed(seed);

		cliffMap = PerlinNoise3D(seed);

		cliffMap.scale = Vec3(0.1);

		biomeMap = LayeredNoise2D(seed);
		biomeMap.scale = Vec2(0.008);

		//cliffMap = PerlinNoise3D(seed);
	}

	void UnloadRegion(Region& region) override {

		using namespace macaron;

		Base64 b64;

		std::stringstream path;
		path << "worlds/0/" << "x" << region.position.x << "_" << "z" << region.position.y << "_" << "world" << ".bin";
		PacketBuilder builder;
		region.toPacket(builder);

		std::stringstream buffer;
		for (int i = 0; i < builder.getLength(); i++) {
			buffer << builder.getData()[i];
		}

		std::string data = b64.Encode(buffer.str());
		json j = {
			{"position",{
				{"x", region.position.x},
				{"z", region.position.y}
			}},
			{"data",data}
		};


		std::ofstream out(path.str());


		//auto bin = json::to_bson(j);
		//for (auto b : bin) out << b;
		//out <<  << std::endl;
		out.close();
	}

	std::shared_ptr<Region> LoadRegion(int x, int z) {

		std::stringstream path;
		path << "worlds/0/" << "x" << x << "_" << "z" << z << "_" << "world" << ".json";

		std::ifstream file(path.str(), std::ifstream::in);

		if (file.good()) {

			json region;

			try {
				file >> region;

				json jdata = region["data"];

				std::string data = jdata.get<std::string>();
				using namespace macaron;

				Base64 b64;

				std::string buffer;

				b64.Decode(data, buffer);

				PacketReader reader(buffer.size(), &buffer[0]);


				std::shared_ptr<Region> ret = std::make_shared<Region>(manager, reader);
				return ret;

			}
			catch (json::exception& e) {

				std::cout << "Exception loading region from file" << std::endl;
				std::cout << e.id << std::endl;
			}
			catch (std::exception& e) {

				std::cout << "Exception loading region from file" << std::endl;
			}


		}

		return std::shared_ptr<Region>();
	}

	std::shared_ptr<Region> GetRegion(int posX, int posZ) override {

		try {

			auto ret = LoadRegion(posX, posZ);
			if (ret) return ret;
			return GenerateRegion(posX, posZ);
		}
		catch (std::bad_alloc& e) {
			std::cout << "Bad alloc" << std::endl;
		}
		catch (std::exception& e) {

		}
	}

	std::shared_ptr<Region> GenerateRegion(int posX, int posZ) {

		std::shared_ptr<Region> ret = std::make_shared<Region>(IVec2(posX, posZ), manager);

		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {
				int blockX = x + (posX * REGION_WIDTH);
				int blockZ = z + (posZ * REGION_LENGTH);

				float biomeHeight = 0;
				float biomeDeviation = 0;
				float cliffWeight = 0;

				for (int i = -3; i <= 3; i++) {
					for (int j = -3; j <= 3; j++) {
						Biome b = getBiomeAt(blockX + i, blockZ + j);
						biomeHeight += b.height;
						biomeDeviation += b.deviation;
						cliffWeight += b.cliffWeight;
					}
				}
				biomeHeight /= 49;
				biomeDeviation /= 49;
				cliffWeight /= 49;

				const int hWeight = 15;
				const int cWeight = 4;



				float height = heightMap.noise(blockX, blockZ);
				//float height = worldMap_.GetValue(blockX * 0.003, blockZ * 0.003, 0.5);


				height *= biomeDeviation;
				height += biomeHeight;


				for (int y = 0; y < REGION_HEIGHT; y++) {

					Block block;

					//int& block = blocks[getIndex(x, y, z)];

					float noiseVal = height - y;
					//noiseVal = p - y;
					//noiseVal = 0;

					if(noiseVal <= biomeDeviation && noiseVal && cliffWeight > 0)
					noiseVal += cliffMap.noise(blockX, y, blockZ) * cliffWeight;

					
					block = (noiseVal >=  0) ? 1 : 0;
					//if (y < 60) block = 2;

					int segment = y / REGION_SEGMENT_HEIGHT;
					ret->volumes[segment].placeBlock(block, Region::getIndex(x, y, z));

				}
			}
		}

		Block dirt = manager.getBlockWithName("dirt").blockId;
		Block rock = manager.getBlockWithName("rock").blockId;
		Block grass = manager.getBlockWithName("grass").blockId;

		Block air = manager.getBlockWithName("air").blockId;
		
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {
				for (int y = 0; y < REGION_HEIGHT; y++) {

					if (ret->getBlock(x, y, z).id) {

						Block block;

						Block y1 = ret->getBlock(x, y + 1, z);
						Block y2 = ret->getBlock(x, y + 2, z);
						Block y3 = ret->getBlock(x, y + 3, z);
						Block y4 = ret->getBlock(x, y + 4, z);

						if (y1 == air) {
							block = grass;
						} else if (y2 == air || y3 == air) {
							block = dirt;	
						} else {
							block = rock;
						}


						ret->placeBlock(block, x, y, z);
					}

				}
			}
		}

		return ret;
	}

};