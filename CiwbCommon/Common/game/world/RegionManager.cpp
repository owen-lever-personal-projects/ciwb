#include "RegionManager.h"

#include <chrono>


void RegionManager::RequestRegionLoad(IVec2 region) {

	/*
	{
		std::lock_guard<std::mutex> lock(loadMutex);
		for (auto it = loadQueue.begin(); it != loadQueue.end(); it++) {
			if (*it == region) {
				return;
			}
		}
	}
	{
		std::lock_guard<std::mutex> lock(readyMutex);
		for (auto it = readyQueue.begin(); it != readyQueue.end(); it++) {
			if ((*it)->position == region) {
				return;
			}
		}
	}
	*/
	{
		std::lock_guard<std::mutex> lock(loadMutex);
		if (loadSet.count(region) == 0) {
			loadSet.insert(region);
			loadQueue.push_back(region);
		}
	}
}

void RegionManager::RequestRegionUnload(std::shared_ptr<Region> region) {
	unloadMutex.lock();
	bool contains = false;
	for (auto it = unloadQueue.begin(); it != unloadQueue.end(); it++) {
		if (*it == region) {
			contains = true;
			break;
		}
	}
	if (!contains) {
		unloadQueue.push_back(region);
	}
	unloadMutex.unlock();
}

std::shared_ptr<Region> RegionManager::Get() {
	readyMutex.lock();

	std::shared_ptr<Region> ret = nullptr;

	while (!ret && readyQueue.size()) {
		ret = readyQueue.front();
		readyQueue.pop_front();
	}
	readyMutex.unlock();

	return ret;
}

void RegionManager::loadLoop() {

	while (running) {

		bool empty = !loadQueue.size();

		if (!empty) {
			IVec2 pos = loadQueue.front();
			auto region = factory.GetRegion(pos.x, pos.y);

			if (region) {
				{
					std::lock_guard<std::mutex> lock(loadMutex);
					loadQueue.pop_front();
					loadSet.erase(pos);
				}
				{
					std::lock_guard<std::mutex> lock(readyMutex);
					readyQueue.push_back(region);
				}
			}

		}
		else {
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(500ms);
		}
	}
}

void RegionManager::unloadLoop() {


	while (running || unloadQueue.size()) {

		bool empty = !unloadQueue.size();

		if (!empty) {
			unloadMutex.lock();
			auto region = unloadQueue.front();
			unloadQueue.pop_front();
			unloadMutex.unlock();

			factory.UnloadRegion(*region);
		}
		else {
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(500ms);
		}
	}
}

RegionManager::RegionManager(RegionFactory& factory) : factory(factory) {
	running = true;
	loadThread = std::thread(&RegionManager::loadLoop, this);
	unloadThread = std::thread(&RegionManager::unloadLoop, this);

}

RegionManager::~RegionManager() {
	running = false;
	loadThread.join();
	unloadThread.join();
}