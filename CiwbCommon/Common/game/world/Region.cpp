#pragma once
// /*

#include "Region.h"

using namespace CiwbEngine;
using namespace Noise;

PoolAllocator Segment::allocator = PoolAllocator(32);


Block Region::getBlock(int x, int y, int z) {
	if (x < 0 || y < 0 || z < 0) return Block(-1);
	if (x >= REGION_WIDTH || y >= REGION_HEIGHT || z >= REGION_LENGTH) return Block(-1);

	int segment = y / REGION_SEGMENT_HEIGHT;

	return volumes[segment][getIndex(x, y, z)];
}

void Region::placeBlock(Block block, int x, int y, int z) {
	if (x < 0 || y < 0 || z < 0) return;
	if (x >= REGION_WIDTH || y >= REGION_HEIGHT || z >= REGION_LENGTH) return;

	int segment = y / REGION_SEGMENT_HEIGHT;

	volumes[segment].placeBlock(block, getIndex(x, y, z));

	//int segment = y / REGION_SEGMENT_HEIGHT;
	y %= REGION_SEGMENT_HEIGHT;
	updateMesh[segment] = true;
	if ((y == 0) && segment) {
		updateMesh[segment - 1] = true;
	}
	else if ((y == REGION_SEGMENT_HEIGHT - 1) && (segment != REGION_VERTICAL_SEGMENTS - 1)) {
		updateMesh[segment + 1] = true;
	}
}



Region::Region(IVec2 position, BlockDataManager& manager) : manager(manager) {
	//blocks.resize((REGION_WIDTH * REGION_HEIGHT * REGION_LENGTH));
	for (bool& b : updateMesh) b = true;
	this->position = position;

	

	//calculateMesh();
}

/*
int Region::getBlockIdNull(int x, int y, int z) {
	return getBlock(x, y, z).id;
}


int Region::getTexture(int id, int face) {
	if (manager) {
		BlockData& block = manager->getBlockWithId(id);
		return block.blockTexture.getTexture(face);
	}
	return -1;
}
*/

void Region::toPacket(PacketBuilder& builder) {
	int lastId;
	int count;
	builder.writeInt(position.x);
	builder.writeInt(position.y);
	for (int y = 0; y < REGION_HEIGHT; y++) {
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {
				//int id = getBlockIdNull(x, y, z);
				//builder.writeInt(id);

				if (x || y || z) {
					int id = getBlock(x, y, z).id;
					if (lastId == id) {
						count++;
					}
					else {
						builder.writeInt(count);
						builder.writeInt(id);
						lastId = id;
						count = 1;
					}
				}
				else {
					int id = getBlock(x, y, z).id;
					builder.writeInt(id);
					lastId = id;
					count = 1;
				}


			}
		}

	}
	builder.writeInt(count);
}

Region::Region(BlockDataManager& manager, PacketReader& reader) : manager(manager){
	//blocks.resize((REGION_WIDTH * REGION_HEIGHT * REGION_LENGTH));
	for (bool& b : updateMesh) b = true;
	this->position.x = reader.readInt();
	this->position.y = reader.readInt();
	int lastId = 0;
	int count = 0;
	for (int y = 0; y < REGION_HEIGHT; y++) {
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {

				if (count) {
					placeBlock(Block(lastId), x, y, z);
					count--;
				}
				else {
					lastId = reader.readInt();
					count = reader.readInt();
					placeBlock(Block(lastId), x, y, z);
					count--;
				}

			}
		}
	}

}
/*
*/