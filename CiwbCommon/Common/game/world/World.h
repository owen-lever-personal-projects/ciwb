#pragma once

#include "Region.h"
#include "Block.h"
#include <map>
#include <list>
#include "Common/engine/maths/maths.h"
#include "Common/engine/noise/NoiseFunction.h"
#include "Common/engine/noise/LayeredNoise2D.h"
#include "Common/game/entities/Player.h"
#include <thread>
#include <list>
#include <atomic>
#include <chrono>
#include <mutex>
#include <memory>
#include "BlockData.h"

using namespace CiwbEngine;
using namespace Noise;


class World {

public:




	virtual std::shared_ptr<Region> getRegion(int x, int z) = 0;

	virtual Block getBlock(float x, float y, float z) = 0;
	virtual void placeBlock(Block, float x, float y, float z) = 0;
	virtual void removeBlock(float x, float y, float z) = 0;

	virtual void Update(float delta) = 0;

	static IVec2 worldCoordsToRegion(float x, float y, float z) {
		IVec2 region;
		region.x = floor(x / REGION_WIDTH);
		region.y = floor(z / REGION_LENGTH);
		return region;
	}

	static IVec2 worldCoordsToRegion(Vec3 vec) {
		return worldCoordsToRegion(vec.x, vec.y, vec.z);
	}

	static Vec3 worldCoordsToRegionCoords(float x, float y, float z) {
		IVec2 regionPos = worldCoordsToRegion(x, y, z);
		x -= regionPos.x * REGION_WIDTH;
		z -= regionPos.y * REGION_LENGTH;
		return Vec3(x, y, z);
	}

	static Vec3 worldCoordsToRegionCoords(Vec3 vec) {
		return worldCoordsToRegionCoords(vec.x, vec.y, vec.z);
	}
};