#pragma once

#include "Region.h"
#include "RegionFactory.h"
#include <memory>
#include <list>
#include <set>
#include <thread>
#include <mutex>

class RegionManager {

private:
	bool running;
	RegionFactory& factory;

	std::mutex readyMutex;
	std::list<std::shared_ptr<Region>> readyQueue;

	std::mutex loadMutex;
	std::list<IVec2> loadQueue;
	std::set<IVec2> loadSet;

	std::mutex unloadMutex;
	std::list<std::shared_ptr<Region>> unloadQueue;

	std::thread loadThread;
	std::thread unloadThread;
	void loadLoop();
	void unloadLoop();

public:
	void RequestRegionLoad(IVec2 req);
	void RequestRegionUnload(std::shared_ptr<Region>);

	std::shared_ptr<Region> Get();


	RegionManager(RegionFactory& factory);
	~RegionManager();



};