#pragma once
 /*

#include "Region.h"

int bsearch(int lo, int hi, int y, std::vector<int>& data) {
	lo >>= 1;
	hi >>= 1;
	int m;
	int x;
	while (lo + 1 < hi) {

		m = (lo + hi) >> 1;
		x = data[m << 1];
		if (x > y) {
			hi = m;
		}
		else if (x < y) {
			lo = m;
		}
		else {
			return m << 1;
		}
	}
	return lo << 1;
}

int getCompressedIndex(int x, int y, int z, std::vector<int>& data) {
	int n = Region::getIndex(x, y, z);
	int lo = 0;
	int hi = (data.size() >> 1) - 1;
	return bsearch(0, data.size(), n, data);
}

Block Region::getBlock(int x, int y, int z) {
	if (x < 0 || y < 0 || z < 0) return Block(-1);
	if (x >= REGION_WIDTH || y >= REGION_HEIGHT || z >= REGION_LENGTH) return Block(-1);

	return blocks[getCompressedIndex(x, y, z, blocks) + 1];
}

void Region::placeBlock(Block block, int x, int y, int z) {
	if (x < 0 || y < 0 || z < 0) return;
	if (x >= REGION_WIDTH || y >= REGION_HEIGHT || z >= REGION_LENGTH) return;

	// TODO
	{
	std::vector<int> uncompressed = uncompress(blocks);
	uncompressed[getIndex(x, y, z)] = block.id;
	blocks = compress(uncompressed);
	}
	// END TODO



	int segment = y / REGION_SEGMENT_HEIGHT;
	y %= REGION_SEGMENT_HEIGHT;
	updateMesh[segment] = true;
	if ((y == 0) && segment) {
		updateMesh[segment - 1] = true;
	}
	else if ((y == REGION_SEGMENT_HEIGHT - 1) && (segment != REGION_VERTICAL_SEGMENTS - 1)) {
		updateMesh[segment + 1] = true;
	}
}



Region::Region(IVec2 position, BlockDataManager* manager, std::shared_ptr<NoiseFunction2D> noise) {
	for (bool& b : updateMesh) b = true;
	this->manager = manager;
	this->position = position;

	std::vector<int> tmp;
	tmp.resize((REGION_WIDTH * REGION_HEIGHT * REGION_LENGTH));
	for (int y = 0; y < REGION_HEIGHT; y++) {
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {

				Block block;

				int blockX = x + (position.x * REGION_WIDTH);
				int blockZ = z + (position.y * REGION_LENGTH);

				block.id = (y - 10 < (10 * noise->noise(blockX, blockZ))) ? 1 : 0;
				if (y < 12) block.id = 2;

				tmp[getIndex(x, y, z)] = block.id;
			}
		}
	}
	blocks = uncompress(tmp);
	//calculateMesh();
}

int Region::getBlockId(int x, int y, int z) {
	return getBlock(x, y, z).id;
}


int Region::getTexture(int id, int face) {
	if (manager) {
		BlockData& block = manager->getBlockWithId(id);
		return block.blockTexture.getTexture(face);
	}
	return -1;
}

void Region::toPacket(PacketBuilder& builder) {
	builder.writeInt(position.x);
	builder.writeInt(position.y);

	for (int i : blocks) {
		builder.writeInt(i);
	}

}

Region::Region(BlockDataManager* manager, PacketReader& reader) {
	//blocks.resize((REGION_WIDTH * REGION_HEIGHT * REGION_LENGTH));
	for (bool& b : updateMesh) b = true;
	this->position.x = reader.readInt();
	this->position.y = reader.readInt();
	this->manager = manager;

	while (reader.remaining() > 4) {
		blocks.push_back(reader.readInt());
	}
}

std::vector<int> Region::compress(const std::vector<int>& data) {
	std::vector<int> blocks;
	int lastId;
	int count;
	for (int y = 0; y < REGION_HEIGHT; y++) {
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {
				if (x || y || z) {
					int id = data[getIndex(x, y, z)];
					if (lastId == id) {
						count++;
					}
					else {
						blocks.push_back(count);
						blocks.push_back(id);
						lastId = id;
						count = 1;
					}
				}
				else {
					int id = data[getIndex(x, y, z)];
					blocks.push_back(id);
					lastId = id;
					count = 1;
				}
			}
		}

	}
	blocks.push_back(count);
	return blocks;
}

std::vector<int> Region::uncompress(const std::vector<int>& data) {
	std::vector<int> blocks;
	blocks.resize((REGION_WIDTH * REGION_HEIGHT * REGION_LENGTH));

	int dataIndex = 0;
	int lastId = 0;
	int count = 0;
	for (int y = 0; y < REGION_HEIGHT; y++) {
		for (int x = 0; x < REGION_WIDTH; x++) {
			for (int z = 0; z < REGION_LENGTH; z++) {
				if (count) {
					blocks[getIndex(x, y, z)] = lastId;
					count--;
				}
				else {
					lastId = data[dataIndex++];
					count = data[dataIndex++];
					blocks[getIndex(x, y, z)] = lastId;
					count--;
				}
			}
		}
	}
	return blocks;
}
/*
*/