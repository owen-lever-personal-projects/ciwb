#pragma once

#include "Common/engine/maths/maths.h"
#include "../../engine/physics/AABB.h"
#include "../../engine/physics/Ray.h"

using namespace CiwbEngine;

struct PlayerInputs {
	static constexpr int WALK_FORWARD = 1 << 0;
	static constexpr int WALK_BACKWARD = 1 << 1;
	static constexpr int WALK_LEFT = 1 << 2;
	static constexpr int WALK_RIGHT = 1 << 3;
	static constexpr int JUMP = 1 << 4;
	static constexpr int PLACE = 1 << 5;
	static constexpr int REMOVE = 1 << 6;
};


class Player {
public:


	static constexpr float WalkSpeed = 10;
	//static constexpr float WalkSpeed = 40;
	static constexpr float FallWalkSpeed = 8;
	//static constexpr float FallWalkSpeed = 40;
	static constexpr float CrouchWalkSpeed = 0.3;
	
	static constexpr float FallAcceleration = 9.8;
	static constexpr float MaxFallSpeed = 53;

	static constexpr float JumpSpeed = 5;
	//static constexpr float JumpSpeed = 100;

	static constexpr float EyeHeight = 1.6;

	static constexpr float PlayerHeight = 1.8;
	static constexpr float PlayerWidth = 0.8;
	static constexpr float PlayerLength = 0.8;
	
	static constexpr float PlayerReach = 6;
	//static constexpr float PlayerReach = 100;




	Vec3 getEyePosition() {
		Vec3 r = position;
		r.y += EyeHeight;
		return r;
	}

	Ray getLookingRay() {
		Vec3 lookingDir = getlookingDirection(hRotation, vRotation);
		lookingDir *= PlayerReach;

		return Ray(getEyePosition(), lookingDir);
	}
	
	


	Vec3 position;
	Vec3 velocity;

	float hRotation;
	float vRotation;
	bool onGround;

	int inputState;
	float fallspeed;
	int selectedBlock = 3;

	void setSelectedBlock(int key) {
		selectedBlock = (selectedBlock * 10 + key) % 1000;
	}

	AABB getHitbox() {
		AABB r;
		r.position = this->position;
		r.position.x -= PlayerWidth / 2;
		r.position.z -= PlayerLength / 2;
		r.size = Vec3(PlayerWidth, PlayerHeight, PlayerLength);
		return r;
	}


	void update(float delta) {

		Vec3 walkDir = Vec3(0);
		if (inputState & (int)PlayerInputs::WALK_FORWARD)  walkDir += getDirFromRotation(hRotation);
		if (inputState & (int)PlayerInputs::WALK_BACKWARD) walkDir -= getDirFromRotation(hRotation);
		if (inputState & (int)PlayerInputs::WALK_LEFT) walkDir += getDirFromRotation(hRotation - 90);
		if (inputState & (int)PlayerInputs::WALK_RIGHT) walkDir += getDirFromRotation(hRotation + 90);

		if(walkDir.length() > 1)
			walkDir.normalise();

		
		if (onGround) walkDir *= WalkSpeed;
		else walkDir *= FallWalkSpeed;

		velocity = (walkDir);
		
		if (onGround) {
			//fallspeed = 0;
			if (inputState & PlayerInputs::JUMP) {
				fallspeed = -JumpSpeed;
				onGround = false;
			}
		}
		else {
			fallspeed += FallAcceleration * delta;
			if (fallspeed > MaxFallSpeed) fallspeed = MaxFallSpeed;

			velocity.y -= fallspeed;

		}

	}

	static Vec3 getDirFromRotation(float rotation) {
		float hRotation = toRadians(rotation);
		Vec3 r(0);
		r.x = sin(hRotation);
		r.z = -cos(hRotation);
		return r;
	}

	static Vec3 getlookingDirection(float hRotation, float vRotation) {
		hRotation = toRadians(hRotation);
		vRotation = toRadians(vRotation);


		float x = sin(hRotation);
		float z = -cos(hRotation);

		float y = sin(vRotation);
		float m = cos(vRotation);

		x *= m;
		z *= m;

		return Vec3(x, y, z);
	}

};