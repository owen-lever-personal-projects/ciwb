#include "DArray.h"

#include <exception>
using namespace CariSTL;

//#define CARI_STL_SAFETY

template <typename T>
T& DArray<T>::operator[](int index) {

#ifdef CARI_STL_SAFETY
	if (index < 0 || index > size) {
		throw std::exception;
	}
#endif

	

	return elements[index];
}

template <typename T>
void DArray<T>::resize(int amount) {
	T newElements[] = new T[amount];

	if (size > amount) size = amount;

	for (int i = 0; i < size; i++) {
		newElements[i] = elements[i];
	}

	delete[] elements;

	elements = newElements;
	capacity = amount;

}

template <typename T>
void DArray<T>::push(T& element) {
	if (this->capacity == this->size) {
		resize(capacity * resizeFactor);
	}
	elements[size] = element;

}

template <typename T>
void DArray<T>::push(T element) {
	if (this->capacity == this->size) {
		resize(capacity * resizeFactor);
	}
	elements[size] = element;
}