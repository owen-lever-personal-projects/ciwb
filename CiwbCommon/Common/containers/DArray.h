#pragma once

namespace CariSTL {

	template <typename T>
	class DArray{

		T *elements;
		int size;
		int capacity;
		
		float resizeFactor = 1.4;


	public:
		DArray() {
			size = 0;
			capacity = 1;
			elements = new T[capacity];
		}

		T& operator[](int index);

		void resize(int amount);

		void push(T& element);
		void push(T element);

	};
}