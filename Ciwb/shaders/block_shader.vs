#version 420 core

layout (location = 0)
in vec4 position;
layout (location = 1)
in vec2 uv;
layout (location = 3)
in vec4 color;
layout (location = 4)
in int id;
layout (location = 2)
in int data;

out vec4 frag_color;
out vec2 frag_uv;
out vec3 frag_normal;
flat out int frag_id;
flat out int frag_render;

uniform mat4 pr_matrix;
uniform mat4 vw_matrix = mat4(1.0);
uniform mat4 ml_matrix = mat4(1.0);
uniform vec3 cameraPos = vec3(0, 100, 0);

vec3 rotate(vec3 v, vec3 axis, float angle);

vec3 normals[8] = {
	vec3(0, 0, -1),
	vec3(0, 0, 1),
	vec3(1, 0, 0),
	vec3(-1, 0, 0),
	vec3(0, 1, 0),
	vec3(0, -1, 0),
	vec3(0, 1, 0),
	vec3(0, 0, 0),
};

float lightVals[8] = {
	0.85,
	0.85,
	0.7,
	0.7,
	1.0,
	0.6,
	1.0,
	1.0,
};

void main()
{
	float centreDepth = -20;
	vec3 centre = vec3(cameraPos.x, centreDepth, cameraPos.z);

	frag_uv = uv;

	vec3 rel = (ml_matrix*vec4(position.xyz, 1)).xyz - centre;

	vec3 axis = vec3(-rel.z, 0, rel.x);
	float angle = acos(dot(normalize(rel), normalize(vec3(0, 1, 0))));
	frag_render = 1;
	axis = normalize(axis);

	vec3 newPos = rotate(rel, axis, angle);
	newPos += centre; 

	//vec4 posRelativeToCam = vw_matrix * vec4(newPos.xyz, 1);

	vec4 posRelativeToCam = vw_matrix * ml_matrix * vec4(position);
	vec4 pos = pr_matrix * posRelativeToCam;

	gl_Position = pos;

	frag_id = id;
	vec3 l = vec3(lightVals[data]);
	frag_color = color * vec4(l.xyz, 1);
	frag_normal = normals[data];

}

//https://gist.github.com/yiwenl/3f804e80d0930e34a0b33359259b556c
mat4 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

vec3 rotate(vec3 v, vec3 axis, float angle) {
	mat4 m = rotationMatrix(axis, angle);
	return (m * vec4(v, 1.0)).xyz;
}