#pragma once


#include <string>
#include <GL/glew.h>
#include "Common/engine/maths/maths.h"


/*
layout (location = 0)
in vec4 position;
layout (location = 1)
in vec2 uv;
layout (location = 2)
in vec3 normal;
layout (location = 3)
in vec4 color;
*/


class Shader
{
public:

	enum CommonShaderAttribLocations {
		position = 0,
		uv = 1,
		normal = 2,
		color = 3,
		id = 4,
		// I don't know why but having data = 5 doesn't work when binding data to the block shader
		// Data in only used in the block shaders which don't have a normal so it's safe to share the same value
		data = 2,
	};

	Shader();
	~Shader();

	void compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);

	void linkShaders();


	void addAttribute(const std::string& attribName);

	void use() const;
	void unuse();

	void setMat4(const std::string name, Mat4* matrix) const;
	void setVec3(const std::string name, Vec3* position) const;
	void setInt(const std::string name, int value) const;
	void setFloat(const std::string name, float value) const;

	GLuint getUniformLocation(const std::string uniformName) const;
	GLuint getAttribLocation(const std::string attribName) const;

private:

	int numAttributes;

	void compileShader(const std::string& filePath, GLuint& id);

	GLuint programId;
	GLuint vertId;
	GLuint fragId;
};

