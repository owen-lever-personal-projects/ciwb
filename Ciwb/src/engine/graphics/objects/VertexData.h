#pragma once
#include "Common/engine/maths/maths.h"


struct Vertex2D {
	Vec3 position;
	Vec4 color;
	Vec2 uv;
};

constexpr int NORTH_NORMAL = 0;
constexpr int SOUTH_NORMAL = 1;
constexpr int EAST_NORMAL = 2;
constexpr int WEST_NORMAL = 3;
constexpr int TOP_NORMAL = 4;
constexpr int BOTTOM_NORMAL = 5;
constexpr int DIAG_NORMAL = 6;
constexpr int NULL_NORMAL = 7;

struct BlockVertex {
	BlockVertex(Vec3 position, Vec2 uv, int id, int data) {
		this->position = position;
		this->uv = uv;
		this->id = id;
		this->data = data;
		this->color = Vec4(1, 1, 1, 1);
	}
	Vec3 position;
	Vec4 color;
	Vec2 uv;
	int id;
	int data;
};

struct Vertex3D {
	Vec3 position;
	Vec2 uv;
	Vec4 color;
	Vec3 normal;
	int id;

	Vertex3D() {
		this->position = Vec3(0, 0, 0);
		this->normal = Vec3(1, 1, 1);
		this->uv = Vec2(0, 0);
		this->color = Vec4(1, 1, 1, 1);
		this->id = 0;
	}
	Vertex3D(Vec3 position, Vec3 normal, Vec2 uv) {
		this->position = position;
		this->normal = normal;
		this->uv = uv;
		this->color = Vec4(1, 1, 1, 1);
		this->id = 0;
	}
	Vertex3D(Vec3 position, Vec3 normal, Vec2 uv, int id) {
		this->position = position;
		this->normal = normal;
		this->uv = uv;
		this->color = Vec4(1, 1, 1, 1);
		this->id = id;
	}



};
struct Vertex3DAnimated {
	Vec3 position;
	Vec2 uv;
	Vec4 color;
	Vec3 normal;
	Vec4 jointIDs;
	Vec4 jointWeights;

	Vertex3DAnimated() {
		this->position = Vec3();
		this->normal = Vec3(1, 1, 1);
		this->uv = Vec2(0, 0);
		this->color = Vec4(1, 1, 1, 1);
		this->jointIDs = Vec4(0, 0, 0, 0);
		this->jointWeights = Vec4(0, 0, 0, 0);
	}

	Vertex3DAnimated(Vec3 position, Vec3 normal, Vec2 uv, Vec4 jointIDs, Vec4 JointWeights) {
		this->position = position;
		this->normal = normal;
		this->uv = uv;
		this->jointIDs = jointIDs;
		this->jointWeights = JointWeights;
		this->color = Vec4(1, 1, 1, 1);
	}
};