#include "RegionMesh.h"
#include <assert.h>


void RegionMesh::draw(int* remaining) {

	generateMesh(remaining);
	for (int i = 0; i < REGION_VERTICAL_SEGMENTS; i++) {
		if (meshes[i].indicesCount) {
			glBindVertexArray(meshes[i].vao);
			glDrawElements(GL_TRIANGLES, meshes[i].indicesCount, GL_UNSIGNED_INT, 0);
			glBindVertexArray(0);
		}
	}


}

std::vector<BlockVertex> RegionMesh::vertices;
std::vector<unsigned int> RegionMesh::indices;

RegionMesh::~RegionMesh() {
	for (MeshProperties mesh : meshes)
	{
		glDeleteBuffers(1, &mesh.ebo);
		glDeleteBuffers(1, &mesh.vbo);
		glDeleteVertexArrays(1, &mesh.vao);
	}
}

static long getTime() {
	return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void RegionMesh::generateMesh(int* remaining) {

	for (int i = 0; i < REGION_VERTICAL_SEGMENTS; i++) {
		if (!region->updateMesh[i]) {
			continue;
		}
		if (remaining) {
			if (*remaining <= 0) {
				return;
			}
			(*remaining)--;
		}

		long start = getTime();

		assert(glGetError() == GL_NO_ERROR);

		MeshProperties& mesh = meshes[i];
		if (mesh.ebo) glDeleteBuffers(1, &mesh.ebo);
		if (mesh.vbo) glDeleteBuffers(1, &mesh.vbo);
		if (mesh.vao) glDeleteVertexArrays(1, &mesh.vao);
		int yOffSet = i * REGION_SEGMENT_HEIGHT;

		assert(glGetError() == GL_NO_ERROR);

		indices.clear();
		vertices.clear();
		//std::cout << "==============" << std::endl;
		//std::cout << "Buffers  :" << getTime()-start << std::endl;

		int quads = 0;

		

		for (int ySeg = 0; ySeg < REGION_SEGMENT_HEIGHT; ySeg++) {
			for (int x = 0; x < REGION_WIDTH; x++) {
				for (int z = 0; z < REGION_LENGTH; z++) {
					int y = ySeg + yOffSet;
					Block block = region->getBlock(x, y, z);
					Vec3 pos(x, y, z);


					if (block.id) {

						auto& bt = region->manager.getBlockWithId(block.id).blockTexture;

						// No block above, render top quad
						if (!region->manager.getBlockWithId(region->getBlock(x, y + 1, z).id).isOpaque) {
							int texId = bt.getTexture(BlockFace::POSY);
							//Top face
							vertices.push_back(BlockVertex(pos + Vec3(0, 1, 0), Vec2(0, 0), texId, TOP_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 1, 1), Vec2(0, -1), texId, TOP_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 1, 0), Vec2(1, 0), texId, TOP_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 1, 1), Vec2(1, -1), texId, TOP_NORMAL));
							quads++;
						}

						// No block below, render bottom quad
						if (!region->manager.getBlockWithId(region->getBlock(x, y - 1, z).id).isOpaque) {
							int texId = bt.getTexture(BlockFace::NEGY);
							//Bottom face
							vertices.push_back(BlockVertex(pos + Vec3(0, 0, 0), Vec2(0, -1), texId, BOTTOM_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 0, 0), Vec2(1, -1), texId, BOTTOM_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 0, 1), Vec2(0, 0), texId, BOTTOM_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 0, 1), Vec2(1, 0), texId, BOTTOM_NORMAL));
							quads++;
						}

						// No block to its right, render right quad
						if (!region->manager.getBlockWithId(region->getBlock(x + 1, y, z).id).isOpaque) {
							int texId = bt.getTexture(BlockFace::POSX);
							//Right face
							vertices.push_back(BlockVertex(pos + Vec3(1, 0, 0), Vec2(1, -1), texId, EAST_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 1, 0), Vec2(1, 0), texId, EAST_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 0, 1), Vec2(0, -1), texId, EAST_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 1, 1), Vec2(0, 0), texId, EAST_NORMAL));
							quads++;
						}

						// No block to its left, render left quad
						if (!region->manager.getBlockWithId(region->getBlock(x - 1, y, z).id).isOpaque) {
							int texId = bt.getTexture(BlockFace::NEGX);
							//Left face
							vertices.push_back(BlockVertex(pos + Vec3(0, 0, 0), Vec2(0, -1), texId, WEST_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 0, 1), Vec2(1, -1), texId, WEST_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 1, 0), Vec2(0, 0), texId, WEST_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 1, 1), Vec2(1, 0), texId, WEST_NORMAL));
							quads++;
						}

						// No block in front, render front quad
						if (!region->manager.getBlockWithId(region->getBlock(x, y, z + 1).id).isOpaque) {
							int texId = bt.getTexture(BlockFace::POSZ);
							//Front face
							vertices.push_back(BlockVertex(pos + Vec3(0, 0, 1), Vec2(0, -1), texId, SOUTH_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 0, 1), Vec2(1, -1), texId, SOUTH_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 1, 1), Vec2(0, 0), texId, SOUTH_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 1, 1), Vec2(1, 0), texId, SOUTH_NORMAL));
							quads++;
						}

						// No block behind, render back quad
						if (!region->manager.getBlockWithId(region->getBlock(x, y, z - 1).id).isOpaque) {
							int texId = bt.getTexture(BlockFace::NEGZ);
							//Back face
							vertices.push_back(BlockVertex(pos + Vec3(0, 0, 0), Vec2(1, -1), texId, NORTH_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(0, 1, 0), Vec2(1, 0), texId, NORTH_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 0, 0), Vec2(0, -1), texId, NORTH_NORMAL));
							vertices.push_back(BlockVertex(pos + Vec3(1, 1, 0), Vec2(0, 0), texId, NORTH_NORMAL));
							quads++;
						}
					}
				}
			}
		}

		//std::cout << "Vertices :" << getTime() - start << std::endl;

		for (int i = 0; i < quads; i++) {
			indices.push_back(i * 4);
			indices.push_back(i * 4 + 1);
			indices.push_back(i * 4 + 2);

			indices.push_back(i * 4 + 3);
			indices.push_back(i * 4 + 2);
			indices.push_back(i * 4 + 1);
		}
		//std::cout << "Indices  :" << getTime() - start << std::endl;


		meshes[i].indicesCount = indices.size();
		//vertices.shrink_to_fit();
		//indices.shrink_to_fit();
		region->updateMesh[i] = false;
		if (!quads) {
			return;
		}

		glGenVertexArrays(1, &meshes[i].vao);
		glGenBuffers(1, &meshes[i].vbo);
		glGenBuffers(1, &meshes[i].ebo);

		glBindVertexArray(meshes[i].vao);
		glBindBuffer(GL_ARRAY_BUFFER, meshes[i].vbo);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(BlockVertex), &vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, meshes[i].ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		GLint pos = Shader::CommonShaderAttribLocations::position;
		glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(BlockVertex), (void*)offsetof(BlockVertex, position));


		glEnableVertexAttribArray(1);
		GLint uv = Shader::CommonShaderAttribLocations::uv;
		glVertexAttribPointer(uv, 2, GL_FLOAT, GL_FALSE, sizeof(BlockVertex), (void*)offsetof(BlockVertex, uv));

		glEnableVertexAttribArray(2);
		GLint color = Shader::CommonShaderAttribLocations::color;
		glVertexAttribPointer(color, 4, GL_FLOAT, GL_FALSE, sizeof(BlockVertex), (void*)offsetof(BlockVertex, color));

		glEnableVertexAttribArray(3);
		GLint id = Shader::CommonShaderAttribLocations::id;
		glVertexAttribIPointer(id, 1, GL_INT, sizeof(BlockVertex), (void*)offsetof(BlockVertex, id));

		glEnableVertexAttribArray(4);
		GLint d = Shader::CommonShaderAttribLocations::data;
		glVertexAttribIPointer(d, 1, GL_INT, sizeof(BlockVertex), (void*)offsetof(BlockVertex, data));

		//std::cout << "Binding  :" << getTime() - start << std::endl;


		glBindVertexArray(0);
	}
}