#include "Texture.h"

#include <iostream>
#include "Common/engine/maths/maths.h"
#include <vector>
#include "Common/engine/utils/Image.h"

using namespace CiwbEngine;

void Texture::bind(unsigned int slot) {
	glBindTexture(GL_TEXTURE_2D + slot, id);
}

void Texture::unbind(unsigned int slot) {
	glBindTexture(GL_TEXTURE_2D + slot, 0);
}

Texture::~Texture() {
	std::cout << "Called texture destructor" << std::endl;
	glDeleteTextures(1, &id);
}


Texture::Texture() {
	id = 0;
	width = 0;
	height = 0;

}

Texture::Texture(int width, int height, uint8_t* data) {
	glGenTextures(1, &id);
	bind();

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glGenerateMipmap(GL_TEXTURE_2D);
	unbind();
}

Texture::Texture(std::string filePath, bool vFlipped) {

	Image image = Image(filePath, vFlipped);
	width = image.width;
	height = image.height;



	glGenTextures(1, &id);
	bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 6);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glGenerateMipmap(GL_TEXTURE_2D);

	unbind();


}