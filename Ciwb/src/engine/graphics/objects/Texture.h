#pragma once

#include <string>
#include <GL/glew.h>

struct Texture {
	int width;
	int height;
	GLuint id;

public:
	//Type type;
	Texture(std::string filePath, bool vFlipped = false);
	Texture(int width, int height, uint8_t* data);
	Texture();
	~Texture();
	void bind(unsigned int slot = 0);
	void unbind(unsigned int slot = 0);
	//static void loadTexture(std::string filePath, int& width, int& height, uint8_t*& bits);

};