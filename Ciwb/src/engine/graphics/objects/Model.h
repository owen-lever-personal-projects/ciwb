#pragma once
#include "../shaders/Shader.h"
#include <vector>
#include "Mesh.h"
#include <string>


namespace CiwbEngine {
	namespace Graphics {

		class Model {
		public:
			Model() {
			}
			Model(const char* path);
			Model(Mesh mesh);

			void draw();

		private:
			std::vector<Mesh> meshes;
			std::string directory;

			void loadModel(const char* path);
			//void processNode(aiNode* node, const aiScene* scene);
			//void processMesh(aiMesh* mesh, const aiScene* scene);
			//std::vector<std::shared_ptr<Texture>> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);


		};




	}
}