#pragma once

#include "Mesh.h"
#include "Common/game/world/Region.h"

class RegionMesh {
	struct MeshProperties {
		GLuint vao, vbo, ebo;
		int indicesCount;

		MeshProperties() {
			vao = 0;
			vbo = 0;
			ebo = 0;
			indicesCount = 0;
		}

	};

	static std::vector<BlockVertex> vertices;
	static std::vector<unsigned int> indices;

public:

	MeshProperties meshes[REGION_VERTICAL_SEGMENTS];
	//bool empty[Region::REGION_VERTICAL_SEGMENTS];
	std::shared_ptr<Region> region;

			
	~RegionMesh();
	RegionMesh(std::shared_ptr<Region> region) {
		this->region = region;
	}

	RegionMesh() {

	}

	void generateMesh(int* remaining = nullptr);
	void draw(int* remaining = nullptr);
};