#include "Mesh.h"
#include <map>

namespace CiwbEngine {
	namespace Graphics {

		Mesh::Mesh(std::vector<Vertex3D> vertices, std::vector<GLuint> indices, std::vector<std::shared_ptr<Texture>> textures) {
			this->vertices = vertices;
			this->indices = indices;
			this->textures = textures;

		}

		void Mesh::deleteMesh() {
			if (setup) {
				glDeleteBuffers(1, &ebo);
				glDeleteBuffers(1, &vbo);
				glDeleteVertexArrays(1, &vao);
			}

			setup = false;
		}

		Mesh::Mesh() {
			setup = false;
		}

		Mesh::~Mesh() {
			deleteMesh();
		}

		void Mesh::setupMesh() {
			if (setup) {
				return;
			}
			setup = true;

			assert(glGetError() == GL_NO_ERROR);

			glGenVertexArrays(1, &vao);
			glGenBuffers(1, &vbo);
			glGenBuffers(1, &ebo);

			glBindVertexArray(vao);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);

			glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex3D), &vertices[0], GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

			glEnableVertexAttribArray(0);
			GLint pos = Shader::CommonShaderAttribLocations::position;
			glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, position));

			glEnableVertexAttribArray(1);
			GLint uv = Shader::CommonShaderAttribLocations::uv;
			glVertexAttribPointer(uv, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, uv));

			glEnableVertexAttribArray(2);
			GLint color = Shader::CommonShaderAttribLocations::color;
			glVertexAttribPointer(color, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, color));

			glEnableVertexAttribArray(3);
			GLint normal = Shader::CommonShaderAttribLocations::normal;
			glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3D), (void*)offsetof(Vertex3D, normal));

			glEnableVertexAttribArray(4);
			GLint id = Shader::CommonShaderAttribLocations::id;
			glVertexAttribIPointer(id, 1, GL_INT, sizeof(Vertex3D), (void*)offsetof(Vertex3D, id));

			glBindVertexArray(0);

			this->indicesCount = indices.size();

			std::vector<Vertex3D>().swap(vertices);
			std::vector<GLuint>().swap(indices);
			//this->textures.clear();

			assert(glGetError() == GL_NO_ERROR);
		}

		void Mesh::draw() {
			//assert(glGetError() == GL_NO_ERROR);
			if (!setup) {
				setupMesh();
			}
			//assert(glGetError() == GL_NO_ERROR);

			for (int i = 0; i < textures.size(); i++) {
				glActiveTexture(GL_TEXTURE0 + i);
				textures[i]->bind();
			}
			glActiveTexture(GL_TEXTURE0);


			glBindVertexArray(vao);
			glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
			glBindVertexArray(0);

			assert(glGetError() == GL_NO_ERROR);
		}


	}
}