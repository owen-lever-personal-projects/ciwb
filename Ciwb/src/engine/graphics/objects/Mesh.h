#pragma once
#include <vector>
#include "VertexData.h"
#include <GL/glew.h>
#include "Texture.h"
#include "../shaders/Shader.h"
#include <assert.h>
namespace CiwbEngine {
	namespace Graphics {

		class Mesh {
		public:
			std::vector<Vertex3D> vertices;
			std::vector<GLuint> indices;
			std::vector<std::shared_ptr<Texture>> textures;

			Mesh(std::vector<Vertex3D> vertices, std::vector<GLuint> indices, std::vector<std::shared_ptr<Texture>> textures);
			void draw();
			void setupMesh();
			void deleteMesh();
			Mesh();
			~Mesh();

		private:
			int indicesCount = 0;
			bool setup = false;
			GLuint vao = 0, vbo = 0, ebo = 0;
		};




	}
}
