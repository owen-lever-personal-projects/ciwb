#pragma once

#include "../objects/Model.h"
#include "Common/engine/maths/maths.h"
#include <GL/glew.h>

namespace CiwbEngine { namespace Graphics {
		class Renderable3D {
		public:
			Renderable3D(Vec3 position, Model* model)
			{
				this->transform = Transform(position);
				this->model = model;
				this->color = Vec4(1, 1, 1, 1);
				this->scale = Vec3(1, 1, 1);
			}

			void draw() const {
				model->draw();
			}

			~Renderable3D() {

			}

			Transform transform;

			Vec3 scale;

			Model* model;
		private:

		protected:
			//Vec2 size;
			Vec4 color;


		};

}}