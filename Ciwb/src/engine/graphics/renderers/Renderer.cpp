#include "Renderer.h"

#include <map>


unsigned int shadowResolution = 1024 * 4;

unsigned int quadVAO;



std::map<Model*, std::deque<Renderable3D*>> models;
std::deque<std::shared_ptr<RegionMesh>> regions;

Mat4 proj;
Mat4 view;

Vec4 skyColor(0.737, 0.886, 1, 1);


Renderer::~Renderer() {

}

Renderer::Renderer() {
	camera = nullptr;

	view = Mat4::identity();

	// Enables MSAA
	glEnable(GL_MULTISAMPLE);

	// Enables alpha blending (transparency)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_ZERO, GL_SRC_COLOR);

	// Enables culling
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glCullFace(GL_BACK);

	// Enables depth testing
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_CLAMP);
	glDepthRange(0.0f, 1.0f);
	glClearDepth(1.0f);

	// Enables alpha testing (Discarding of fragments that don't have greater than 0 alpha)
	glAlphaFunc(GL_GREATER, 0);
	glEnable(GL_ALPHA_TEST);


	blockShader.compileShaders("shaders/block_shader.vs", "shaders/block_shader.fs");
	blockShader.linkShaders();

	objectShader.compileShaders("shaders/object_shader.vs", "shaders/object_shader.fs");
	objectShader.linkShaders();

	// Sets the width and height values to the default resolution
	this->width = 800;
	this->height = 450;



	// Binds the default frame buffer and texture after the shadowmap has been setup
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glBindTexture(GL_TEXTURE_2D, 0);
	//standardShader.use();

	// Sets the uniform location of "tex" and "shadowMap" to 0 and 1 in the gpu
	//int t0 = standardShader.getUniformLocation("tex");
	//glUniform1i(t0, 0);

	//initPostProcessingVAO(quadVAO, postProcessShader);

}


void Renderer::begin() {
}

void Renderer::end() {
}

void Renderer::draw(Renderable3D& renderable) {
	models[renderable.model].push_back(&renderable);
}

void Renderer::draw(std::shared_ptr<RegionMesh> region) {
	regions.push_back(region);
}

void Renderer::setProjectionMatrix(Mat4* matrix) {
	proj = *matrix;
}

void Renderer::setViewMatrix(Mat4* matrix) {
	view = *matrix;
}
		
void Renderer::resize(int width, int height) {
	//resizeFBO(&scene, width, height);
	this->width = width;
	this->height = height;
}




void Renderer::flush() {
	glEnable(GL_DEPTH_TEST);
	blockShader.use();
			
	if (camera)	setViewMatrix(&camera->getView());

	blockShader.setVec3("viewPos", camera ? &camera->getPosition() : &Vec3());
	blockShader.setMat4("vw_matrix", &view);
	blockShader.setMat4("pr_matrix", &proj);
	blockShader.setVec3("cameraPos", &camera->getPosition());

	objectShader.use();
	objectShader.setVec3("viewPos", camera ? &camera->getPosition() : &Vec3());
	objectShader.setMat4("vw_matrix", &view);
	objectShader.setMat4("pr_matrix", &proj);


			

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(skyColor.x, skyColor.y, skyColor.z, skyColor.w);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, width, height);

			

	glDisable(GL_BLEND);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_EQUAL, 1);


	blockShader.use();
	RenderRegions(&blockShader);

	objectShader.use();
	Render3D(&objectShader);
			

	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ZERO, GL_SRC_COLOR);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glAlphaFunc(GL_LESS, 1);

	glDepthMask(GL_TRUE);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/*
	glCullFace(GL_FRONT);
	edgeShader.use();
	edgeShader.setMat4("vw_matrix", &view);
	edgeShader.setMat4("pr_matrix", &proj);
	Render3D(&edgeShader);
	glCullFace(GL_BACK);
	*/
	objectShader.use();
	Render3D(&objectShader);
			
			
	glDisable(GL_DEPTH_TEST);

	//glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);


	ResetBuffers();
}


void Renderer::Render3D(Shader* shader) {

	for (std::map<Model*, std::deque<Renderable3D*>>::iterator it = models.begin(); it != models.end(); it++) {
		Model* model = it->first;
		std::deque<Renderable3D*>& queue = models[model];
		for (unsigned int i = 0; i < queue.size(); i++) {
			Renderable3D* renderable = queue[i];
			Mat4 matrix = Mat4::scale(renderable->scale);
			matrix.multiply(renderable->transform.getMat4());
			shader->setMat4("ml_matrix", &matrix);

			renderable->draw();
		}
	}

}

void Renderer::RenderRegions(Shader* shader) {

	texture->bind();
	int meshCreateCount = 1;
	for (std::shared_ptr<RegionMesh> mesh : regions) {

				
		Vec3 pos;
		pos.x += mesh->region->position.x * REGION_WIDTH;
		pos.z += mesh->region->position.y * REGION_LENGTH;

		Mat4 matrix = Mat4::translation(pos);
		shader->setMat4("ml_matrix", &matrix);
		mesh->draw(&meshCreateCount);

	}
}

/*
void Renderer::RenderRegions(Shader* shader) {
	for (Region* region : regions) {

		for (int x = 0; x < Region::REGION_WIDTH; x++) {
			for (int y = 0; y < Region::REGION_HEIGHT; y++) {
				for (int z = 0; z < Region::REGION_LENGTH; z++) {
					Block& block = region->getBlock(x, y, z);
					if (block.id) {

						Vec3 pos = Vec3(x, y, z);
						pos.x += region->position.x * Region::REGION_WIDTH;
						pos.z += region->position.y * Region::REGION_LENGTH;

						Mat4 matrix =Mat4::translation(pos);
						shader->setMat4("ml_matrix", &matrix);
						region->draw();
								
						this->cube->transform.position = pos;

						Mat4 matrix = cube->transform.getMat4();
						shader->setMat4("ml_matrix", &matrix);
						cube->draw();
								

					}

				}
			}
		}
	}
}
*/

void Renderer::setCamera(Camera* camera) {
	this->camera = camera;
}


		
void Renderer::ResetBuffers() {
	models.clear();
	regions.clear();
}