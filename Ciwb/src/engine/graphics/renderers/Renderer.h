#pragma once


#include "../shaders/Shader.h"
#include "Common/engine/maths/maths.h"
#include "../../core/Window.h"
#include "../renderables/Renderable3D.h"
#include "../objects/RegionMesh.h"
#include "../camera/Camera.h"

#include "Common/game/world/Region.h"

using namespace CiwbEngine;
using namespace Graphics;


#include <deque>
class Renderer {
public:

	Shader blockShader;
	Shader objectShader;

	Renderer();
	~Renderer();
	void begin();
	void end();

	std::shared_ptr<Texture> texture;

			
	void draw(Renderable3D& renderable);
	void draw(std::shared_ptr<RegionMesh> region);

	void setProjectionMatrix(Mat4* matrix);
	void setViewMatrix(Mat4* matrix);
	void setCamera(Camera* camera);

	void resize(int width, int height);

	void flush();
private:
	int width;
	int height;
	Camera* camera;

	void Render3D(Shader* shader);
	void RenderRegions(Shader* shader);

	void ResetBuffers();
			

};


