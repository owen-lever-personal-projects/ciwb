#include "InputHandler.h"
#include <vector>

namespace CiwbEngine {
	namespace Input {



		/*
			-1 Key is not held down
			0 key was pressed and released same tick
			1 key was pressed
			1+ nth tick this key was held down for

		*/
		float lastX;
		float lastY;

		bool InputHandler::wasKeyPressed(int key) {
			if (keys.find(key) != keys.end()) {
				int value = keys[key];
				return (value == 1);
			}
			else {
				return false;
			}
		}

		int InputHandler::isKeyDown(int key) {
			if (keys.find(key) != keys.end()) {
				int value = keys[key];
				return value;
			}
			else {
				return 0;
			}
		}

		bool InputHandler::wasMousePressed(int button) {
			if (mouseButtons.find(button) != mouseButtons.end()) {
				int value = mouseButtons[button];
				return (value == 1);
			}
			else {
				return false;
			}
		}

		int InputHandler::isMouseDown(int button) {
			if (mouseButtons.find(button) != mouseButtons.end()) {
				int value = mouseButtons[button];
				return value;
			}
			else {
				return 0;
			}
		}


		void InputHandler::update() {
			double nx;
			double ny;
			glfwGetCursorPos(window, &nx, &ny);
			xpos = nx - lastX;
			ypos = ny - lastY;
			lastX = nx;
			lastY = ny;

			for (auto it = mouseButtons.begin(); it != mouseButtons.end(); it++) {
				it->second++;
				/*
				int b = it->first;
				int v = it->second;
				if (v > 0) {
					mouseButtons[b] = v + 1;
				}
				else if (v == 0) {
					mouseButtons[b] = -1;
				}
				*/
			}


			for (auto it = keys.begin(); it != keys.end(); it++) {
				it->second++;
				/*
				int k = it->first;
				int v = it->second;
				if (v > 0) {
					keys[k] = v + 1;
				}
				else if (v == 0) {
					keys[k] = -1;
				}
				*/
			}
		}

		void InputHandler::cursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
			this->xpos = (float)xpos;
			this->ypos = (float)ypos;
		}

		void InputHandler::enableCursor() {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}

		void InputHandler::disableCursor() {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			if (glfwRawMouseMotionSupported()) {
				glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
			}
		}

		void InputHandler::hideCursor() {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		}

		void InputHandler::setWindow(GLFWwindow* window) {
			this->window = window;
		}

		void InputHandler::setScreenSize(int width, int height) {
			this->screenWidth = width;
			this->screenHeight = height;
		}

		void InputHandler::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
			if (action == GLFW_PRESS) {
				keys[key] = 0;
			}
			else if (action == GLFW_RELEASE) {
				keys.erase(key);
			}
		}

		void InputHandler::mouseCallback(GLFWwindow* window, int button, int action, int mods) {
			if (action == GLFW_PRESS) {
				mouseButtons[button] = 0;
			}
			else if (action == GLFW_RELEASE) {
				mouseButtons.erase(button);
			}
		}



	}
}