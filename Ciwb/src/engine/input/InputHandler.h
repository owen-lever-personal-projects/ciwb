#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <map>

namespace CiwbEngine {
	namespace Input {

		class InputHandler {
		public:


			bool wasKeyPressed(int key);
			int isKeyDown(int key);
			
			bool wasMousePressed(int key);
			int isMouseDown(int key);
			
			void update();
			float xpos;
			float ypos;



			void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
			void mouseCallback(GLFWwindow* window, int button, int action, int mods);
			void cursorPosCallback(GLFWwindow* window, double xpos, double ypos);

			void enableCursor();
			void disableCursor();
			void hideCursor();

			void setWindow(GLFWwindow* window);
			void setScreenSize(int width, int height);
		private:
			GLFWwindow* window;
			std::map<int, int> keys;
			std::map<int, int> mouseButtons;
			int screenWidth, screenHeight;





		};
	}
}