#pragma once

#include "Window.h"
#include "../graphics/renderers/Renderer.h"
#include "../input/InputHandler.h"

using namespace CiwbEngine;
using namespace Graphics;
using namespace Input;

class Game {

public:


	Window* window;
	Renderer* renderer;
	InputHandler* inputHandler;

	virtual void Resize(int width, int height) = 0;
	virtual void Init() = 0;
	virtual void Load() = 0;
	virtual void Draw() = 0;
	virtual void Update() = 0;
	virtual void Exit() = 0;

	int tps, fps;
};
