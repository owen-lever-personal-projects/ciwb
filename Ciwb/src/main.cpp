#include "game/Ciwb.h"
#include "engine/core/Application.h"
#include <ENET/enet.h>

int main(void)
{
    Ciwb game;
    Application app = Application(&game);
    app.init();
    app.run();

    return 0;
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    PSTR lpCmdLine, INT nCmdShow)
{
    return main();
}
