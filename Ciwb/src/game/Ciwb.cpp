#include "Ciwb.h"
#include <vector>
#include <random>
#include <time.h>
#include "Common/engine/maths/maths.h"

#include "../engine/graphics/objects/Mesh.h"
#include "../engine/graphics/objects/Texture.h"
#include "../engine/graphics/camera/Camera.h"

#include "World/ClientWorld.h"
#include "World/MultiPlayerClientWorld.h"
#include "World/SinglePlayerClientWorld.h"

#include <memory>

#include <thread>
#include <list>
#include <ctime>

#include "Common/engine/networking/NetworkHandler.h"


float hRotation = 0;
float vRotation = 0;
float sensitivity = 0.1f;
float movespeed = 0.6f;

std::shared_ptr<Texture> texture;
ClientWorld* world;

using namespace CiwbEngine;

// Perspective matrix
Mat4 pers;

// Orthogonal matrix
Mat4 ortho;

// Boolean to determine whether to use the perspective matrix (true) or orthogonal matrix (false)
bool usePers;

FpsCam camera = FpsCam(Vec3(0));

ClientConnection connection;

int renderDistance = 8;


bool inFocus;
void Ciwb::Init() {
	inFocus = true;
	//while (!connection.connectToServer("172.16.4.218", 8080));
	//while (!connection.connectToServer("localhost", 8080));

	long seed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	std::cout << seed << std::endl;
	world = new SinglePlayerClientWorld(seed);
	world->renderDistance = renderDistance;

	world->player.position = Vec3(-3, 200, 3);

	Resize(800, 450);
}

void Ciwb::Exit() {

	std::cout << "Game exit" << std::endl;
	delete world;
}

void Ciwb::Resize(int width, int height) {
	// Re-calculates the projection matrices to fit the new aspect ratio
	float aspr = (float)width / float(height);
	pers = Mat4::persp(90, aspr, 0.01, 60000);
	//pers = Mat4::persp(90/aspr, aspr, 1, 6000);
	
	ortho = Mat4::ortho(60, -60, -60 * aspr, 60 * aspr, 0, 1000);
	


}

void Ciwb::Load() {

	texture = std::make_shared<Texture>("atlas.png");

	renderer->texture = texture;

	world->player.position = Vec3(0, 50, 0);

	inputHandler->disableCursor();
}


void Ciwb::Draw() {


	camera.position = world->player.getEyePosition();
	camera.setRotation(world->player.hRotation, world->player.vRotation);

	IVec2 pos;
	pos.x = floor(world->player.position.x / REGION_WIDTH);
	pos.y = floor(world->player.position.z / REGION_LENGTH);

	int draw = 0;

	auto r = world->getRegionMesh(pos.x, pos.y);
	if (r) {
		renderer->draw(r);
		draw++;
	}


	for (int i = 0; i < renderDistance; i++) {
		int l = i * 2 + 2;

		for (int j = 0; j < l; j++) {
			IVec2 loc = IVec2(-i +  j, -i - 1) + pos;
			auto region = world->getRegionMesh(loc.x, loc.y);
			if (region) {
				renderer->draw(region);
				draw++;
			}
		}
		for (int j = 0; j < l; j++) {
			IVec2 loc = IVec2(i + 1, -i + j) + pos;
			auto region = world->getRegionMesh(loc.x, loc.y);
			if (region) {
				renderer->draw(region);
				draw++;
			}
		}
		for (int j = 0; j < l; j++) {
			IVec2 loc = IVec2(i - j, i + 1) + pos;
			auto region = world->getRegionMesh(loc.x, loc.y);
			if (region) {
				renderer->draw(region);
				draw++;
			}
		}
		for (int j = 0; j < l; j++) {
			IVec2 loc = IVec2(-i - 1, i - j) + pos;
			auto region = world->getRegionMesh(loc.x, loc.y);
			if (region) {
				renderer->draw(region);
				draw++;
			}

		}
	}

	


	renderer->setCamera(&camera);
	renderer->setProjectionMatrix(&pers);
}

void Ciwb::Update() {


	world->renderDistance = renderDistance;
	
	if (inFocus) {

		float camspeed = 0.8f;

		if (inputHandler->isKeyDown('=')) movespeed *= 1.1f;
		if (inputHandler->isKeyDown('-')) movespeed /= 1.1f;

		camspeed = movespeed;
		world->player.inputState = 0;
	
		if (inputHandler->isKeyDown('D')) 
			world->player.inputState |= PlayerInputs::WALK_RIGHT;

		if (inputHandler->isKeyDown('A'))
			world->player.inputState |= PlayerInputs::WALK_LEFT;
	
		if (inputHandler->isKeyDown('W'))
			world->player.inputState |= PlayerInputs::WALK_FORWARD;

		if (inputHandler->isKeyDown('S'))
			world->player.inputState |= PlayerInputs::WALK_BACKWARD;

		if (inputHandler->isKeyDown(GLFW_KEY_SPACE))
			world->player.inputState |= PlayerInputs::JUMP;

		if (inputHandler->isKeyDown(GLFW_KEY_R))
			world->player.position.y = 300;

		if (inputHandler->wasMousePressed(0))
			world->player.inputState |= PlayerInputs::REMOVE;

		if (inputHandler->wasMousePressed(1))
			world->player.inputState |= PlayerInputs::PLACE;

		static bool pressedLast;

		bool pressed = false;
		int key;
		for (int i = 0; i < 9; i++) {
			if (inputHandler->isKeyDown(GLFW_KEY_0 + i)) {
				pressed = true;
				key = i;
			}
		}
		if (pressed && !pressedLast) {
			world->player.setSelectedBlock(key);
		}
		pressedLast = pressed;

		std::cout << world->player.selectedBlock << std::endl;


		world->player.hRotation += inputHandler->xpos * sensitivity;
		world->player.vRotation -= inputHandler->ypos * sensitivity;

		if (world->player.vRotation < -90.0f) world->player.vRotation = -90.0f;
		if (world->player.vRotation > 90.0f) world->player.vRotation = 90.0f;
	}

	//if (inputHandler->isDown(GLFW_KEY_LEFT_SHIFT)) camera.position += camera.getDown() * camspeed;


	// Old keyboard camera controls using arrow keys
	//if (inputHandler->isDown(GLFW_KEY_UP)) vRotation++;
	//if (inputHandler->isDown(GLFW_KEY_DOWN)) vRotation--;

	//if (inputHandler->isDown(GLFW_KEY_LEFT)) hRotation--;
	//if (inputHandler->isDown(GLFW_KEY_RIGHT)) hRotation++;


	
	world->Update(1.0f / 60.0f);
	

	//if (inputHandler->isKeyDown(GLFW_KEY_ESCAPE)) window->close();
	if (inputHandler->isKeyDown(GLFW_KEY_ESCAPE)) {
		inputHandler->enableCursor();
		inFocus = false;
	}
	if (inputHandler->isMouseDown(0)) {
		inputHandler->disableCursor();
		inFocus = true;
	}

}

