#pragma once

#include "Common/game/world/World.h"
#include "../../engine/graphics/objects/RegionMesh.h"

#include "../../engine/graphics/renderers/Renderer.h"
#include "ClientWorld.h"

#include "Common/game/world/RegionFactory.h"
#include "Common/game/world/RegionManager.h"


#include <set>








class SinglePlayerClientWorld : public ClientWorld {

	std::string worldName;
	int counter;

	ClassicRegionFactory factory;
	RegionManager regionManager;

	void queueRegionsLoad();
	
	bool closed;

	void SaveAndClose();




	
public:

	SinglePlayerClientWorld(long seed) : factory(seed, manager), regionManager(factory) {

		closed = false;
	}

	~SinglePlayerClientWorld() override{
		closed = true;
		SaveAndClose();
	}


	virtual void Draw(Renderer& renderer) override;

	virtual bool IsClosed() override;

	virtual std::shared_ptr<Region> getRegion(int x, int z) override;
	virtual std::shared_ptr<RegionMesh> getRegionMesh(int x, int z) override;

	virtual Block getBlock(float x, float y, float z) override;
	virtual void placeBlock(Block, float x, float y, float z) override;
	virtual void removeBlock(float x, float y, float z) override;

	virtual void Update(float delta) override;








};