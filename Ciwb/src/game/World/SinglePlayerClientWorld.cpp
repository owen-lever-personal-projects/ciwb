#include "SinglePlayerClientWorld.h"


void SinglePlayerClientWorld::Draw(Renderer& renderer) {

}

bool SinglePlayerClientWorld::IsClosed() {
	return closed;
}

std::shared_ptr<Region> SinglePlayerClientWorld::getRegion(int x, int z) {
	IVec2 position(x, z);

	std::shared_ptr<Region> ret;

	if (regions.count(position)) {
		ret = regions[position];
	}

	return ret;
}

std::shared_ptr<RegionMesh> SinglePlayerClientWorld::getRegionMesh(int x, int z) {
	IVec2 position(x, z);

	std::shared_ptr<RegionMesh> ret;

	if (meshes.count(position)) {

		if (regions.count(position)) {
			ret = meshes[position];
		} else {
			meshes.erase(position);
			ret = nullptr;
		}
		

	} else {
		if (regions.count(position)) {
			meshes[position] = std::make_shared<RegionMesh>(regions[position]);
			ret = meshes[position];
		}
		else {
			ret = nullptr;
		}
	}
	return ret;
}

Block SinglePlayerClientWorld::getBlock(float x, float y, float z) {
	IVec2 pos = World::worldCoordsToRegion(x, y, z);
	auto region = getRegion(pos.x, pos.y);
	if (region) {
		Vec3 relativeCoords = World::worldCoordsToRegionCoords(x, y, z);
		return region->getBlock(relativeCoords.x, relativeCoords.y, relativeCoords.z);
	}
	
	return Block(-1);
}

void SinglePlayerClientWorld::placeBlock(Block block, float x, float y, float z) {
	IVec2 pos = World::worldCoordsToRegion(x, y, z);
	auto region = getRegion(pos.x, pos.y);
	if (region) {
		Vec3 relativeCoords = World::worldCoordsToRegionCoords(x, y, z);
		region->placeBlock(block, relativeCoords.x, relativeCoords.y, relativeCoords.z);
	}
}

void SinglePlayerClientWorld::removeBlock(float x, float y, float z) {
	IVec2 pos = World::worldCoordsToRegion(x, y, z);
	auto region = getRegion(pos.x, pos.y);
	if (region) {
		Vec3 relativeCoords = World::worldCoordsToRegionCoords(x, y, z);
		region->placeBlock(Block(0), relativeCoords.x, relativeCoords.y, relativeCoords.z);
	}
}

static long getTimeNow() {
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void SinglePlayerClientWorld::Update(float delta) {
	counter++;

	queueRegionsLoad();


	player.update(delta);

	IVec2 posBeforeUpdate = World::worldCoordsToRegion(player.position);


	if (player.inputState & PlayerInputs::REMOVE || player.inputState & PlayerInputs::PLACE) {
		Ray ray = player.getLookingRay();
		AABB rayAABB = ray.getAABB();

		int blockX, blockY, blockZ;
		float distance = FP_INFINITE;

		bool hit = false;

		NormalDirection direction = NormalDirection::UNKNOWN;

		for (int x = floor(rayAABB.getLeft()); x <= floor(rayAABB.getRight()); x++) {
			for (int y = floor(rayAABB.getBottom()); y <= floor(rayAABB.getTop()); y++) {
				for (int z = floor(rayAABB.getFront()); z <= floor(rayAABB.getBack()); z++) {
					Block block = getBlock(x, y, z);
					if (manager.getBlockWithId(block.id).isSolid) {
						AABB blockHitbox;
						blockHitbox.size = Vec3(1);
						blockHitbox.position = Vec3(x, y, z);

						RayAABBCollisionResult result = ray.interect(blockHitbox);
						if (result.collides) {
							if (result.min < distance) {
								distance = result.min;
								blockX = x;
								blockY = y;
								blockZ = z;
								direction = result.normal;
								hit = true;
							}
						}
					}

				}
			}
		}

		if (distance > 0) {

			if (hit && player.inputState & PlayerInputs::REMOVE) {
				this->removeBlock(blockX, blockY, blockZ);
			}

			if (hit && player.inputState & PlayerInputs::PLACE) {
				if (direction == NormalDirection::POSX) blockX++;
				if (direction == NormalDirection::NEGX) blockX--;

				if (direction == NormalDirection::POSY) blockY++;
				if (direction == NormalDirection::NEGY) blockY--;

				if (direction == NormalDirection::POSZ) blockZ++;
				if (direction == NormalDirection::NEGZ) blockZ--;

				AABB blockHitbox;
				blockHitbox.size = Vec3(1);
				blockHitbox.position = Vec3(blockX, blockY, blockZ);

				if (!player.getHitbox().collides(blockHitbox).collides) {
					this->placeBlock(Block(player.selectedBlock), blockX, blockY, blockZ);

				}
			}
		}


	}

	// Do world-player collision detection and response

	Vec3 velocity = player.velocity * delta;
	AABB hitbox;

	player.onGround = false;

	player.position.z += velocity.z;
	hitbox = player.getHitbox();
	for (int x = floor(hitbox.getLeft()); x <= floor(hitbox.getRight()); x++) {
		for (int y = floor(hitbox.getBottom()); y <= floor(hitbox.getTop()); y++) {
			for (int z = floor(hitbox.getFront()); z <= floor(hitbox.getBack()); z++) {
				Block block = getBlock(x, y, z);

				if (manager.getBlockWithId(block.id).isSolid) {
					AABB blockHitbox;
					blockHitbox.size = Vec3(1);
					blockHitbox.position = Vec3(x, y, z);

					CollisionResult r = player.getHitbox().checkZInterect(blockHitbox);
					if (r.collides) {
						player.position.z += r.normalBtoA.z * r.depth;
					}

				}


			}
		}
	}


	player.position.x += velocity.x;
	hitbox = player.getHitbox();
	for (int x = floor(hitbox.getLeft()); x <= floor(hitbox.getRight()); x++) {
		for (int y = floor(hitbox.getBottom()); y <= floor(hitbox.getTop()); y++) {
			for (int z = floor(hitbox.getFront()); z <= floor(hitbox.getBack()); z++) {
				Block block = getBlock(x, y, z);

				if (manager.getBlockWithId(block.id).isSolid) {
					AABB blockHitbox;
					blockHitbox.size = Vec3(1);
					blockHitbox.position = Vec3(x, y, z);

					CollisionResult r = player.getHitbox().checkXInterect(blockHitbox);
					if (r.collides) {
						Vec3 pushVec = r.normalBtoA * r.depth;
						//std::cout << pushVec << std::endl;
						player.position.x += pushVec.x;
					}

				}


			}
		}
	}

	player.position.y += velocity.y;
	hitbox = player.getHitbox();
	for (int x = floor(hitbox.getLeft()); x <= floor(hitbox.getRight()); x++) {
		for (int y = floor(hitbox.getBottom()); y <= floor(hitbox.getTop()); y++) {
			for (int z = floor(hitbox.getFront()); z <= floor(hitbox.getBack()); z++) {
				Block block = getBlock(x, y, z);

				if (manager.getBlockWithId(block.id).isSolid) {
					AABB blockHitbox;
					blockHitbox.size = Vec3(1);
					blockHitbox.position = Vec3(x, y, z);

					CollisionResult r = player.getHitbox().checkYInterect(blockHitbox);
					if (r.collides) {
						Vec3 pushVec = r.normalBtoA * r.depth;
						player.position.y += pushVec.y;

						if (r.normalBtoA == Vec3(0, 1, 0)) {
							player.onGround = true;
							if (player.fallspeed > 0) player.fallspeed = 0;
						}

						if (r.normalBtoA == Vec3(0, -1, 0)) {
							if (player.fallspeed < 0) player.fallspeed = 0;
						}
					}

				}


			}
		}
	}

	IVec2 posAfterUpdate = World::worldCoordsToRegion(player.position);
	
}

void SinglePlayerClientWorld::SaveAndClose() {
	for (auto& pair : regions) {
		std::shared_ptr<Region> region = pair.second;
		regionManager.RequestRegionUnload(region);
	}
	regions.clear();
}






void SinglePlayerClientWorld::queueRegionsLoad() {

	if (counter % (60 / 20) == 0) {
		auto region = regionManager.Get();

		if (region) {
			regions.insert(std::make_pair(region->position, region));
		}
	}

	IVec2 pos = World::worldCoordsToRegion(player.position);

	std::set<IVec2> inRangeRegions;

	std::list<IVec2> toLoad;
	std::list<IVec2> toUnload;


	for (int i = -renderDistance; i <= renderDistance; i++) {
		for (int j = -renderDistance; j <= renderDistance; j++) {
			inRangeRegions.insert(pos + IVec2(i, j));
		}
	}

	for (auto& r : regions) {
		if (!inRangeRegions.count(r.first)) {
			toUnload.push_back(r.first);
		}
		else {
			inRangeRegions.erase(r.first);
		}
	}

	for (auto& r : inRangeRegions) {
		toLoad.push_back(r);
	}

	toLoad.sort(
		[pos](IVec2 f, IVec2 s) {
			f -= pos;
			s -= pos;

			return (f.x * f.x + f.y * f.y) < (s.x * s.x + s.y * s.y);
		}
	);

	for (auto& r : toLoad) {
		regionManager.RequestRegionLoad(r);
	}

	for (auto& r : toUnload) {
		auto region = regions[r];
		regions.erase(r);
		regionManager.RequestRegionUnload(region);
		meshes.erase(r);
	}
}
