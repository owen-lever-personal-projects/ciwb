#pragma once

#include "Common/game/world/World.h"
#include "../../engine/graphics/objects/RegionMesh.h"
#include "../../engine/graphics/renderers/Renderer.h"

class ClientWorld : public World{


protected:
	BlockDataManager manager;

	std::map<IVec2, std::shared_ptr<Region>> regions;
	std::map<IVec2, std::shared_ptr<RegionMesh>> meshes;




public:
	Player player;
	int renderDistance;

	virtual ~ClientWorld() {

	}

	virtual void Draw(Renderer& renderer) = 0;

	virtual bool IsClosed() = 0;



	virtual std::shared_ptr<Region> getRegion(int x, int z) = 0;
	virtual std::shared_ptr<RegionMesh> getRegionMesh(int x, int z) = 0;




};