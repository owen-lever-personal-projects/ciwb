#include "Common/game/world/World.h"
#include "Common/engine/networking/NetworkHandler.h"
#include "../../engine/graphics/objects/RegionMesh.h"
#include "../../engine/graphics/renderers/Renderer.h"

#include "ClientWorld.h"


using namespace CiwbEngine;
using namespace Graphics;

class MultiPlayerClientWorld : public ClientWorld, public NetworkHandler {
	ClientConnection* connection;


	std::list<IVec2> meshGenQueue;

	std::list<IVec2> recentlyRequested;
	std::mutex recentlyRequestedMutex;

	std::thread meshGenThread;
	std::mutex meshGenMutex;
	std::mutex unloadQueueMutex;

	std::mutex regionsMutex;
	std::atomic<bool> running;


	void requestRegion(IVec2 position);

public:
	int renderDistance = 2;

	void meshGenLoop();
	MultiPlayerClientWorld() {

	}
	~MultiPlayerClientWorld() {

	}
	MultiPlayerClientWorld(ClientConnection* client);

	Block getBlock(float x, float y, float z);
	void placeBlock(Block, float x, float y, float z);
	void removeBlock(float x, float y, float z);


	void RecievePacket(NetworkUser user, int length, const char* data);

	void OnConnect(NetworkUser user);
	void OnDisconnect(NetworkUser user);

	void Update(float time);
	void Draw(Renderer& renderer);

	bool IsClosed();

	std::shared_ptr<Region> getRegion(int x, int z) override;
	std::shared_ptr<RegionMesh> getRegionMesh(int x, int z) override;
};