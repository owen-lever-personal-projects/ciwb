#include "MultiPlayerClientWorld.h"
#include <vector>

void MultiPlayerClientWorld::requestRegion(IVec2 position) {

	recentlyRequestedMutex.lock();
	for (IVec2 region : recentlyRequested) {
		if (region == position) {
			recentlyRequestedMutex.unlock();
			return;
		}
	}
	recentlyRequested.push_back(position);
	recentlyRequestedMutex.unlock();


	PacketBuilder builder;
	builder.writeShort(PacketHeaders::ToServer::RegionSubscriptionRequest);
	builder.writeInt(position.x);
	builder.writeInt(position.y);

	connection->sendPacket(builder.getLength(), builder.getData());
}

MultiPlayerClientWorld::MultiPlayerClientWorld(ClientConnection* connection) {
	this->connection = connection;
	connection->setNetworkHandler(this);

	running = true;
	meshGenThread = std::thread(&MultiPlayerClientWorld::meshGenLoop, this);
	//TODO
}

void MultiPlayerClientWorld::meshGenLoop() {
	while (running) {
		bool empty = true;

		if (!meshGenQueue.empty()) {
			meshGenMutex.lock();
			IVec2 vec = meshGenQueue.front();
			meshGenQueue.pop_front();
			meshGenMutex.unlock();

			if (regions.count(vec) && (meshes.count(vec) == 0)) {
				std::shared_ptr<Region> region = regions[vec];
				meshes[vec] = std::make_shared<RegionMesh>(region);
			}
			else {
				auto a = getRegion(vec.x, vec.y);
			}
		}
		//std::chrono::milliseconds time(20);
		//std::this_thread::sleep_for(time);
	}
}

void MultiPlayerClientWorld::Draw(Renderer& renderer) {

}

bool MultiPlayerClientWorld::IsClosed() {
	return !running;
}


Block MultiPlayerClientWorld::getBlock(float x, float y, float z) {
	IVec2 pos = World::worldCoordsToRegion(x, y, z);
	auto region = getRegion(pos.x, pos.y);
	if (region) {
		Vec3 relativeCoords = World::worldCoordsToRegionCoords(x, y, z);
		return region->getBlock(relativeCoords.x, relativeCoords.y, relativeCoords.z);
	}
	return Block(-1);
}

void MultiPlayerClientWorld::placeBlock(Block block, float x, float y, float z) {
	
	PacketBuilder builder;
	builder.writeShort(PacketHeaders::ToServer::BlockUpdate);
	builder.writeInt(block.id);
	builder.writeInt(floor(x));
	builder.writeInt(floor(y));
	builder.writeInt(floor(z));

	connection->sendPacket(builder.getLength(), builder.getData());
	
}

void MultiPlayerClientWorld::removeBlock(float x, float y, float z) {
	placeBlock(Block(0), x, y, z);
}

void MultiPlayerClientWorld::RecievePacket(NetworkUser user, int length, const char* data) {
	PacketReader reader(length, data);
	uint16_t header = reader.readShort();
	if (header == PacketHeaders::FromServer::RegionDataResponse) {
		std::shared_ptr<Region> region = std::make_shared<Region>(manager, reader);
		IVec2 position = region->position;
		regionsMutex.lock();
		regions.insert(std::pair<IVec2, std::shared_ptr<Region>>(position, region));
		regionsMutex.unlock();

		recentlyRequestedMutex.lock();
		recentlyRequested.remove(position);
		recentlyRequestedMutex.unlock();
		//std::cout << "RECEIVED REGION RESPONSE" << position.x << " : " << position.y << std::endl;
	}
	if (header == PacketHeaders::FromServer::BlockUpdate) {
		std::cout << length << std::endl;
		Block block = Block(reader.readInt());

		int x = reader.readInt();
		int y = reader.readInt();
		int z = reader.readInt();
		IVec2 regionPos = World::worldCoordsToRegion(x, y, z);
		auto region = getRegion(regionPos.x, regionPos.y);
		if (region) {
			Vec3 relativeCoords = World::worldCoordsToRegionCoords(x, y, z);
			region->placeBlock(block, relativeCoords.x, relativeCoords.y, relativeCoords.z);
		}
	}
	if (header == PacketHeaders::FromServer::RegionSubscriptionResponse) {
		IVec2 region;
		region.x = reader.readInt();
		region.y = reader.readInt();
		int response = reader.readInt();

		if (response) {
			PacketBuilder builder;
			builder.writeShort(PacketHeaders::ToServer::RegionDataRequest);
			builder.writeInt(region.x);
			builder.writeInt(region.y);
			connection->sendPacket(builder.getLength(), builder.getData());
		}

		recentlyRequestedMutex.lock();
		recentlyRequested.remove(region);
		recentlyRequestedMutex.unlock();

	}
}

void MultiPlayerClientWorld::OnConnect(NetworkUser user) {

}
void MultiPlayerClientWorld::OnDisconnect(NetworkUser user) {

}

std::shared_ptr<RegionMesh> MultiPlayerClientWorld::getRegionMesh(int x, int z) {
	IVec2 position(x, z);

	if (meshes.count(position) == 0) {
		meshGenMutex.lock();
		bool queue = true;
		for (IVec2 r : meshGenQueue) {
			if (r == position) {
				queue = false;
			}
		}
		if (queue) meshGenQueue.push_back(position);
		meshGenMutex.unlock();
		return nullptr;
	}

	return meshes[position];
}

std::shared_ptr<Region> MultiPlayerClientWorld::getRegion(int x, int z) {
	regionsMutex.lock();
	IVec2 position(x, z);
	if (regions.count(position) == 0) {
		regionsMutex.unlock();
		requestRegion(position);
		return nullptr;
	}
	regionsMutex.unlock();
	return regions[position];
}

void MultiPlayerClientWorld::Update(float delta) {

	regionsMutex.lock();
	if (regions.count(World::worldCoordsToRegion(player.position)) == 0) {
		regionsMutex.unlock();
		//std::cout << "Pausing updates, waiting for region to load" << std::endl;
		return;
	}
	regionsMutex.unlock();

	//std::list<IVec2> toUnload;
	regionsMutex.lock();
	for (auto r = regions.begin(); r != regions.end(); r++) {
		IVec2 pos = r->first;
		
		IVec2 loc = World::worldCoordsToRegion(player.position);
		//std::cout << player.position << std::endl;
		//std::cout << loc << std::endl;
		IVec2 dist = pos - loc;
		if (dist.x < 0) dist.x *= -1;
		if (dist.y < 0) dist.y *= -1;
		if ((dist.x > renderDistance) || (dist.y > renderDistance)) {
				
				regions.erase(pos);
				meshGenMutex.lock();
				if (meshes.count(pos)) {
					meshes.erase(pos);
				}
				meshGenMutex.unlock();
				PacketBuilder builder;
				builder.writeShort(PacketHeaders::ToServer::RegionSubscriptionCancelation);
				builder.writeInt(pos.x);
				builder.writeInt(pos.y);
				connection->sendPacket(builder.getLength(), builder.getData());
				break;
     			//toUnload.push_back(r->first);
		}
		
	}

	regionsMutex.unlock();

	// Update player states
	player.update(delta);

	if (player.inputState & PlayerInputs::REMOVE || player.inputState & PlayerInputs::PLACE) {
		Ray ray = player.getLookingRay();
		AABB rayAABB = ray.getAABB();

		int blockX, blockY, blockZ;
		float distance = FP_INFINITE;

		bool hit = false;

		NormalDirection direction = NormalDirection::UNKNOWN;

		for (int x = floor(rayAABB.getLeft()); x <= floor(rayAABB.getRight()); x++) {
			for (int y = floor(rayAABB.getBottom()); y <= floor(rayAABB.getTop()); y++) {
				for (int z = floor(rayAABB.getFront()); z <= floor(rayAABB.getBack()); z++) {
					Block block = getBlock(x, y, z);
					if (manager.getBlockWithId(block.id).isSolid) {
						AABB blockHitbox;
						blockHitbox.size = Vec3(1);
						blockHitbox.position = Vec3(x, y, z);

						RayAABBCollisionResult result = ray.interect(blockHitbox);
						if (result.collides) {
							if (result.min < distance) {
								distance = result.min;
								blockX = x;
								blockY = y;
								blockZ = z;
								direction = result.normal;
								hit = true;
							}
						}
					}
					
				}
			}
		}

		if (distance > 0) {

			if (hit && player.inputState & PlayerInputs::REMOVE) {
				this->removeBlock(blockX, blockY, blockZ);
			}

			if (hit && player.inputState & PlayerInputs::PLACE) {
				if (direction == NormalDirection::POSX) blockX++;
				if (direction == NormalDirection::NEGX) blockX--;

				if (direction == NormalDirection::POSY) blockY++;
				if (direction == NormalDirection::NEGY) blockY--;

				if (direction == NormalDirection::POSZ) blockZ++;
				if (direction == NormalDirection::NEGZ) blockZ--;

				AABB blockHitbox;
				blockHitbox.size = Vec3(1);
				blockHitbox.position = Vec3(blockX, blockY, blockZ);

				if (!player.getHitbox().collides(blockHitbox).collides) {
					this->placeBlock(Block(3), blockX, blockY, blockZ);
					
				}
			}
		}


	}

	// Do world-player collision detection and response
	
	Vec3 velocity = player.velocity * delta;
	AABB hitbox;
	
	player.onGround = false;

	player.position.z += velocity.z;
	hitbox = player.getHitbox();
	for (int x = floor(hitbox.getLeft()); x <= floor(hitbox.getRight()); x++) {
		for (int y = floor(hitbox.getBottom()); y <= floor(hitbox.getTop()); y++) {
			for (int z = floor(hitbox.getFront()); z <= floor(hitbox.getBack()); z++) {
				Block block = getBlock(x, y, z);

				if (manager.getBlockWithId(block.id).isSolid) {
					AABB blockHitbox;
					blockHitbox.size = Vec3(1);
					blockHitbox.position = Vec3(x, y, z);

					CollisionResult r = player.getHitbox().checkZInterect(blockHitbox);
					if (r.collides) {
						player.position.z += r.normalBtoA.z * r.depth;
					}

				}


			}
		}
	}

	player.position.x += velocity.x;
	hitbox = player.getHitbox();
	for (int x = floor(hitbox.getLeft()); x <= floor(hitbox.getRight()); x++) {
		for (int y = floor(hitbox.getBottom()); y <= floor(hitbox.getTop()); y++) {
			for (int z = floor(hitbox.getFront()); z <= floor(hitbox.getBack()); z++) {
				Block block = getBlock(x, y, z);

				if (manager.getBlockWithId(block.id).isSolid) {
					AABB blockHitbox;
					blockHitbox.size = Vec3(1);
					blockHitbox.position = Vec3(x, y, z);

					CollisionResult r = player.getHitbox().checkXInterect(blockHitbox);
					if (r.collides) {
						Vec3 pushVec = r.normalBtoA * r.depth;
						//std::cout << pushVec << std::endl;
						player.position.x += pushVec.x;
					}

				}


			}
		}
	}

	player.position.y += velocity.y;
	hitbox = player.getHitbox(); 
	for (int x = floor(hitbox.getLeft()); x <= floor(hitbox.getRight()); x++) {
		for (int y = floor(hitbox.getBottom()); y <= floor(hitbox.getTop()); y++) {
			for (int z = floor(hitbox.getFront()); z <= floor(hitbox.getBack()); z++) {
				Block block = getBlock(x, y, z);

				if (manager.getBlockWithId(block.id).isSolid) {
					AABB blockHitbox;
					blockHitbox.size = Vec3(1);
					blockHitbox.position = Vec3(x, y, z);

					CollisionResult r = player.getHitbox().checkYInterect(blockHitbox);
					if (r.collides) {
						Vec3 pushVec = r.normalBtoA * r.depth;
						player.position.y += pushVec.y;
	
						if (r.normalBtoA == Vec3(0, 1, 0)) {
							player.onGround = true;
							if (player.fallspeed > 0) player.fallspeed = 0;
						}

						if (r.normalBtoA == Vec3(0, -1, 0)) {
							if (player.fallspeed < 0) player.fallspeed = 0;
						}
					}

				}


			}
		}
	}

	PacketBuilder builder;
	builder.writeShort(PacketHeaders::ToServer::PlayerPositionUpdate);
	builder.writeFloat(player.position.x);
	builder.writeFloat(player.position.y);
	builder.writeFloat(player.position.z);
	builder.writeFloat(player.hRotation);
	builder.writeFloat(player.vRotation);
	connection->sendPacket(builder.getLength(), builder.getData());
	
}