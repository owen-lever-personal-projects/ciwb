#pragma once

#include "../engine/core/Game.h"



class Ciwb : public Game {

	void Init() override;
	void Load() override;
	void Draw() override;
	void Update() override;
	void Exit() override;
	void Resize(int width, int height) override;



};
