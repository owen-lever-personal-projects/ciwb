#version 400



out vec4 color;


in vec4 frag_color;
in vec2 frag_uv;
in vec3 frag_normal;
flat in int frag_id;

uniform sampler2D tex;
void main()
{
	//if(frag_id == -1) discard;
	
	int tileW = 128;
	int tileH = 128;
	int texW = 512;
	int texH = 256;

	int rows = texW / tileW;
	int cols = texH / tileH;

	int x = frag_id % rows;
	int y = frag_id / rows;

	vec2 texPos = vec2(float(x) / float(rows), float(y) / float(cols));
	vec2 texSize = vec2(float(tileW) / float(texW), float(tileH) / float(texH));


	

	vec2 calcFragUv = texPos + frag_uv * texSize;

	color = frag_color;
	//color *= texture(tex, texPos + texSize * frag_uv);
	color *= texture(tex, frag_uv);
	//if(color.a != 1) discard;

	vec3 norm = vec3(normalize(frag_normal));
}
