#version 330 core

layout (location = 0)
in vec4 position;
layout (location = 1)
in vec2 uv;
layout (location = 2)
in vec3 normal;
layout (location = 3)
in vec4 color;
layout (location = 4)
in int id;

out vec4 frag_color;
out vec2 frag_uv;
out vec3 frag_normal;
flat out int frag_id;

uniform mat4 pr_matrix;
uniform mat4 vw_matrix = mat4(1.0);
uniform mat4 ml_matrix = mat4(1.0);


void main()
{
	frag_uv = uv;
	
	vec4 posRelativeToCam = vw_matrix * ml_matrix * position;
	vec4 pos = pr_matrix * posRelativeToCam;

	gl_Position = pos;

	frag_id = id;

	frag_color = color;
	frag_normal = mat3(transpose(inverse(ml_matrix))) * normal;

}