#include "ServerWorld.h"



ServerWorld::ServerWorld(ServerConnection* connection, long seed) {
	this->seed = seed;
	this->server = connection;
	float lac = 2;
	float pers = 0.5;
	this->noise = std::make_shared<LayeredNoise2D>(LayeredNoise2D(seed, lac, pers, 3));
	noise->scale = Vec2(0.1 / pi, 0.1 / pi);
	connection->setNetworkHandler(this);

	this->running = true;
	this->loadThread = std::thread(&ServerWorld::loadLoop, this);
	this->regionResponseThread = std::thread(&ServerWorld::regionResponseLoop, this);
}

void ServerWorld::requestRegionLoad(IVec2 position) {
	loadQueueMutex.lock();
	loadQueue.push_back(position);
	loadQueueMutex.unlock();
}
void ServerWorld::requestRegionUnload(IVec2 position) {
	loadQueueMutex.lock();
	unloadQueue.push_back(position);
	loadQueueMutex.unlock();

}
void ServerWorld::OnConnect(NetworkUser user) {
	Player* player = new Player();
	auto pair = std::pair<NetworkUser, Player*>(user, player);
	players.insert(pair);
}
void ServerWorld::OnDisconnect(NetworkUser user) {
	Player* player = players[user];
	players.erase(user);
	delete player;
}


void ServerWorld::RecievePacket(NetworkUser user, int length, const char* data) {
	PacketReader reader(length, data);
	uint16_t header = reader.readShort();

	if (header == PacketHeaders::ToServer::RegionDataRequest) {
		IVec2 regionLocation;
		regionLocation.x = reader.readInt();
		regionLocation.y = reader.readInt();

		Player* player = players[user];
		bool contains = false;

		for (IVec2 region : playerSubscriptions[player]) {
			if (region == regionLocation) {
				contains = true;
				break;
			}
		}

		if (contains) {

			if (regions.count(regionLocation) == 0) {
				requestRegionLoad(regionLocation);
			}
			// QUEUE REGION RESPONSE
			regionResponceMutex.lock();
			std::list<NetworkUser>& users = regionResponseQueue[regionLocation];
			bool contains = false;
			for (NetworkUser& u : users) {
				if (u == user) contains = true;
			}
			if (!contains) {
				regionResponseQueue[regionLocation].push_back(user);
			}
			regionResponceMutex.unlock();
		}


	}
	if (header == PacketHeaders::ToServer::BlockUpdate) {
		Block block = Block(reader.readInt());

		int x = reader.readInt();
		int y = reader.readInt();
		int z = reader.readInt();

		placeBlock(block, x, y, z);
		
	}
	if (header == PacketHeaders::ToServer::PlayerPositionUpdate) {
		Player* player = players[user];
		player->position.x = reader.readFloat();
		player->position.y = reader.readFloat();
		player->position.z = reader.readFloat();
		player->hRotation = reader.readFloat();
		player->vRotation = reader.readFloat();
	}
	if (header == PacketHeaders::ToServer::RegionSubscriptionRequest) {
		Player* player = players[user];
		IVec2 region;
		region.x = reader.readInt();
		region.y = reader.readInt();
		IVec2 distance = worldCoordsToRegion(player->position) - region;
		if (distance.x < 0) distance.x *= -1;
		if (distance.y < 0) distance.y *= -1;
		int dist = distance.x + distance.y;

		PacketBuilder builder;
		builder.writeShort(PacketHeaders::FromServer::RegionSubscriptionResponse);
		builder.writeInt(region.x);
		builder.writeInt(region.y);
		if (dist > maxRenderDistance) {
			builder.writeInt(false);
		}
		else {
			builder.writeInt(true);
			regionSubscriptions[region].push_back(player);
			playerSubscriptions[player].push_back(region);
		}
		server->sendPacket(user, builder.getLength(), builder.getData());
	}
	if (header == PacketHeaders::ToServer::RegionSubscriptionCancelation) {
		Player* player = players[user];
		IVec2 region;
		region.x = reader.readInt();
		region.y = reader.readInt();
		regionSubscriptions[region].remove(player);
		playerSubscriptions[player].remove(region);

		if (!regionSubscriptions[region].size()) {
			unloadQueueMutex.lock();
			unloadQueue.push_back(region);
			unloadQueueMutex.unlock();
		}

	}
}


std::shared_ptr<Region> ServerWorld::getRegion(int x, int z) {
	regionsMutex.lock();
	IVec2 position(x, z);
	if (regions.count(position) == 0) {
		regionsMutex.unlock();
		requestRegionLoad(position);
		return nullptr;
	}
	regionsMutex.unlock();
	return regions[position];
}


Block ServerWorld::getBlock(float x, float y, float z) {
	IVec2 pos = worldCoordsToRegion(x, y, z);
	auto region = getRegion(pos.x, pos.y);
	if (region) {
		return region->getBlock(x, y, z);
	}
	return Block(0);
}
void ServerWorld::placeBlock(Block toPlace, float x, float y, float z) {

	IVec2 pos = worldCoordsToRegion(x, y, z);
	auto region = getRegion(pos.x, pos.y);
	if (region) {
		Vec3 relativeCoords = World::worldCoordsToRegionCoords(x, y, z);
		region->placeBlock(toPlace, relativeCoords.x, relativeCoords.y, relativeCoords.z);
		PacketBuilder builder;
		builder.writeShort(PacketHeaders::FromServer::BlockUpdate);
		builder.writeInt(toPlace.id);
		builder.writeInt(x);
		builder.writeInt(y);
		builder.writeInt(z);
		server->sendPacketToAll(builder.getLength(), builder.getData());

		//for(auto it = plater)
		
	}
	
}

void ServerWorld::regionResponseLoop() {
	while (running) {

		if (!regionResponseQueue.empty()) {
			IVec2 pos;
			bool toLoad = false;
			regionResponceMutex.lock();
			for (auto it = regionResponseQueue.begin(); it != regionResponseQueue.end(); it++) {
				pos = it->first;
				if (regions.count(pos)) {
					toLoad = true;
					break;
				}
			}
			regionResponceMutex.unlock();
			if (toLoad) {
				regionResponceMutex.lock();
				std::list<NetworkUser> users = regionResponseQueue[pos];
				regionResponseQueue.erase(pos);
				regionResponceMutex.unlock();

				PacketBuilder builder;
				builder.writeShort(PacketHeaders::FromServer::RegionDataResponse);
				regions[pos]->toPacket(builder);
				for (NetworkUser user : users) {
					server->sendPacket(user, builder.getLength(), builder.getData());
				}
			}

		}
	}
}

void ServerWorld::loadLoop() {
	while (running) {
		bool empty = true;
		//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		if (!loadQueue.empty()) {
			loadQueueMutex.lock();
			IVec2 vec = loadQueue.front();
			loadQueue.pop_front();
			loadQueueMutex.unlock();

			if (regions.count(vec) == 0) {
				std::cout << "Creating Region at : " << vec << std::endl;
				std::shared_ptr<Region> region = std::make_shared<Region>(vec, &manager, noise);
				regionsMutex.lock();
				regions.insert(std::pair<IVec2, std::shared_ptr<Region>>(vec, region));
				regionsMutex.unlock();
			}
			empty = false;
		}

		if (!unloadQueue.empty()) {
			unloadQueueMutex.lock();
			IVec2 vec = loadQueue.front();
			loadQueue.pop_front();
			unloadQueueMutex.unlock();

			if (regions.count(vec)) {
				regionsMutex.lock();
				regions.erase(vec);
				regionsMutex.unlock();
			}
			empty = false;
		}
		if (empty) {
			std::chrono::milliseconds time(20);
			std::this_thread::sleep_for(time);
		}
	}
}

void ServerWorld::update(float delta) {






}
