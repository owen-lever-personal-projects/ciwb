#include "Common/game/world/World.h"
#include "Common/engine/networking/NetworkHandler.h"

class ServerWorld : public World, public NetworkHandler{
	ServerConnection* server;

	long seed;
	std::shared_ptr<NoiseFunction2D> noise;
	
	std::map<IVec2, std::shared_ptr<Region>> regions;
	BlockDataManager manager;
	
	std::map<NetworkUser, Player*> players;

	std::map<Player*, std::list<IVec2>> playerSubscriptions;
	std::map<IVec2, std::list<Player*>> regionSubscriptions;

	std::list<IVec2> loadQueue;
	std::list<IVec2> unloadQueue;

	std::map<IVec2, std::list<NetworkUser>> regionResponseQueue;
	std::mutex regionResponceMutex;

	std::thread loadThread;
	std::thread regionResponseThread;
	std::mutex loadQueueMutex;
	std::mutex unloadQueueMutex;

	std::mutex regionsMutex;
	std::atomic<bool> running;


	void requestRegionLoad(IVec2 position);
	void requestRegionUnload(IVec2 position);

	void loadLoop();
	void regionResponseLoop();
	int maxRenderDistance = 8;


public:
	ServerWorld(ServerConnection* server, long seed);

	std::shared_ptr<Region> getRegion(int x, int z);

	Block getBlock(float x, float y, float z);
	void placeBlock(Block, float x, float y, float z);

	void update(float delta);

	void RecievePacket(NetworkUser user, int length, const char* data);
	
	void OnConnect(NetworkUser user);
	void OnDisconnect(NetworkUser user);
};