#include <ENET/enet.h>

#include <iostream>
#include "Common/engine/networking/NetworkHandler.h"

#include "Common/game/world/World.h"
#include "game/world/ServerWorld.h"
#include <chrono>

class NetworkHandler2 : NetworkHandler {
    ServerConnection* server;
    void RecievePacket(NetworkUser user, int length, const char* data) override {
        std::cout << data << std::endl;
        const char* received = "Recieved";
        server->sendPacket(user, strlen(received) + 1, received);
    }
    void OnConnect(NetworkUser user) override {
        std::cout << "C" << std::endl;
    }
    void OnDisconnect(NetworkUser user) override {
        std::cout << "D" << std::endl;
    }
public:
    NetworkHandler2(ServerConnection* server) {
        this->server = server;
    }
};

long getTimeMillis() {

	auto now = std::chrono::system_clock::now();
	auto now_ms = std::chrono::time_point_cast<std::chrono::milliseconds>(now);
	auto epoch = now_ms.time_since_epoch();
	auto value = std::chrono::duration_cast<std::chrono::milliseconds>(epoch);
	long duration = value.count();
	return duration;
}


int main(void)
{
    
    ServerConnection server;
    server.setupServer(8080);

    //NetworkHandler2 handler(&server);
   // server.setNetworkHandler((NetworkHandler*)&handler);
    long seed = 1729;
    ServerWorld world(&server, seed);


	int tps = 30;
	long mpt = 1000 / tps;
	
	int seconds = 0;
	int updates = 0;

	long lastTick = getTimeMillis();
	while (true) {

		long time = getTimeMillis();
		long ticksToNextUpdate = (lastTick + mpt) - time;
		if (ticksToNextUpdate <= 0) {
			lastTick += mpt;

			world.update(float(mpt) / 1000);
			updates++;


			if (updates == tps) {
				updates = 0;
				seconds++;
			}
		}
		else {
			std::chrono::milliseconds wait(ticksToNextUpdate);

			std::this_thread::sleep_for(wait);
		}
	}
    
    

}

/*
void Application::run() {
	game->Load();
	int tps = 60;

	// Sets the time per tick to 1/60th of a second
	double tpt = 1.0 / (double)tps;

	// Sets the lastTick value used for timing to the amount of ticks elapsed since the engine was initialised
	int lastTick = (int)(glfwGetTime() / tpt);

	int second = 0;
	int frames = 0;
	int updates = 0;

	while (!window->closed()) {
		double time = glfwGetTime();
		int tick = time / tpt;

		// Updates the game object if the current tick value is greater than lastTick
		if (tick > lastTick) {
			update();
			updates++;
			lastTick++;
		}
		// Draws the game if it does not need to be updated
		else {
			frames++;
			draw();
		}
		// Updates the game's tps and fps counter every second
		if ((int)time > second) {
			second = (int)time;
			game->tps = updates;
			game->fps = frames;
			updates = 0;
			frames = 0;
		}
	}
	game->Exit();

}
*/